package pt.isel.deetc.leic.jcp4e.jcdk.version;
import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IParameterRule;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IsOptional;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.RequireNoValue;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.RequireValue;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.ProfileCommands;


public class JCDK222 {
	private static Parameter getNewParameter(String option, String label)
	{
		Parameter p1 = new Parameter();
		p1.setParameterOption(option);
		p1.setParameterLabel(label);
		return p1;
	}

	private static void commandSetting (Command c, String option, String label, IParameterRule r)
	{
		Parameter p1 = getNewParameter(option, label);
		p1.addParameterRule(new IsOptional());
		p1.addParameterRule(r);
		c.addParameter(p1);
	}

	public static Command getScriptGen()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.scriptgen.Main");
		command.setCommandLabel("Script Generator");
		command.setName("ScriptGen");

		commandSetting(command, "-help", "Help", new RequireNoValue());
		commandSetting(command, "-version", "Version", new RequireNoValue());
		commandSetting(command, "-nobanner", "NoBanner", new RequireNoValue());
		commandSetting(command, "-nobeginend", " Suppress 'CAP_BEGIN', 'CAP_END' APDU commands.", new RequireNoValue());
		commandSetting(command, "-o", "Output File", new RequireValue());
		commandSetting(command, "-package", "Package name.", new RequireValue());

		return command;	
	}
	public static Command getCapDump()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.capdump.CapDump");
		command.setCommandLabel("Cap Dump");
		command.setName("CapDump");
		return command;	
	}
	
	public static Command getCapGen()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.jcasm.cap.Main");
		command.setCommandLabel("Cap Gen");
		command.setName("Capgen");
		commandSetting(command, "-help", "Help", new RequireNoValue());
		commandSetting(command, "-version", "Version", new RequireNoValue());
		commandSetting(command, "-nobanner", "NoBanner", new RequireNoValue());
		commandSetting(command, "-o", "Output File", new RequireValue());
		return command;	
	}
	
	public static Command getVerifyCap()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.offcardverifier.Verifier");
		command.setCommandLabel("Verify Cap");
		command.setName("Verifycap");
		commandSetting(command, "-help", "Help", new RequireNoValue());
		commandSetting(command, "-version", "Version", new RequireNoValue());
		commandSetting(command, "-nobanner", "NoBanner", new RequireNoValue());
		commandSetting(command, "-nowarn", "NoWarn", new RequireNoValue());
		commandSetting(command, "-verbose", "Verbose", new RequireNoValue());
		commandSetting(command, "-package", "Package", new RequireValue());
		return command;	
	}
	public static Command getVerifyExp()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.offcardverifier.VerifyExp");
		command.setCommandLabel("Verify Exp");
		command.setName("VerifyExp");
		commandSetting(command, "-help", "Help", new RequireNoValue());
		commandSetting(command, "-version", "Version", new RequireNoValue());
		commandSetting(command, "-nobanner", "NoBanner", new RequireNoValue());
		commandSetting(command, "-nowarn", "NoWarn", new RequireNoValue());
		commandSetting(command, "-verbose", "Verbose", new RequireNoValue());
		return command;	
	}
	
	public static Command getVerifyRev()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.offcardverifier.VerifyRev");
		command.setCommandLabel("Verify Rev");
		command.setName("VerifyRev");
		commandSetting(command, "-help", "Help", new RequireNoValue());
		commandSetting(command, "-version", "Version", new RequireNoValue());
		commandSetting(command, "-nobanner", "NoBanner", new RequireNoValue());
		commandSetting(command, "-nowarn", "NoWarn", new RequireNoValue());
		commandSetting(command, "-verbose", "Verbose", new RequireNoValue());
		return command;	
	}
	
	
	public static Command getConverter()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.converter.Converter");
		command.setCommandLabel("Converter");
		command.setName("Converter");

		commandSetting(command, "-classdir", "ClassDir", new RequireValue());
		commandSetting(command, "-i", "32bit Integer", new RequireNoValue());
		commandSetting(command, "-exportpath", "ExportPath", new RequireValue());
		commandSetting(command, "-exportmap", "ExportMap", new RequireValue());
		commandSetting(command, "-applet", "Applet AID", new RequireValue());
		commandSetting(command, "-d", "Root Output Directory", new RequireValue());
		commandSetting(command, "-Version", "Version", new RequireNoValue());
		commandSetting(command, "-verbose", "Verbose", new RequireNoValue());
		commandSetting(command, "-help", "Help", new RequireNoValue());
		commandSetting(command, "-nowarn", "No Warn", new RequireNoValue());
		commandSetting(command, "-mask", "Mask", new RequireNoValue());
		commandSetting(command, "-debug", "Debug", new RequireNoValue());
		commandSetting(command, "-nobanner", "No Banner", new RequireNoValue());
		commandSetting(command, "-noverify", "No Verify", new RequireNoValue());

		return command;	
	}
	
	public static Command getApduTool()
	{
		Command command = new Command();
		command.setCommandEntryPoint("com.sun.javacard.apdutool.Main");
		command.setCommandLabel("Apdu Tool");
		command.setName("ApduTool");

		commandSetting(command, "-verbose", "Verbose", new RequireNoValue());
		commandSetting(command, "-nobanner", "NoBanner", new RequireNoValue());
		commandSetting(command, "-noatr", "No Atr", new RequireNoValue());
		commandSetting(command, "-o", "Output File", new RequireValue());
		commandSetting(command, "-h", "Host Name", new RequireValue());
		commandSetting(command, "-p", "Port", new RequireValue());
		commandSetting(command, "-s", "Serial Port", new RequireValue());
		commandSetting(command, "-version", "Version", new RequireNoValue());
		commandSetting(command, "-t0", "T0", new RequireNoValue());

		return command;	
	}
	
	public static ProfileCommands getCommandProfile() {
	
		Command c1 = getApduTool();
		Command c2 = getScriptGen();
		Command c3 = getCapDump();
		Command c4 = getCapGen();
		Command c5 = getConverter();
		Command c6 = getVerifyCap();
		Command c7 = getVerifyExp();
		Command c8 = getVerifyRev();
		
		ProfileCommands cp = new ProfileCommands();
		cp.add(c1);
		cp.add(c2);
		cp.add(c3);
		cp.add(c4);
		cp.add(c5);
		cp.add(c6);
		cp.add(c7);
		cp.add(c8);
		
		return cp;
	}
	public static void main(String[] args) {
		Profile p = new Profile();
		p.setLibPath("/home/nac/Applications/JCKITs/jc222/lib/");
		String classPathFiles = p.getClassPathFiles();
		Command c1 = getApduTool();
		Command c2 = getScriptGen();
		Command c3 = getCapDump();
		Command c4 = getCapGen();
		Command c5 = getConverter();
		Command c6 = getVerifyCap();
		Command c7 = getVerifyExp();
		Command c8 = getVerifyRev();
		c1.setClassPathFiles(classPathFiles);
		c2.setClassPathFiles(classPathFiles);
		c3.setClassPathFiles(classPathFiles);
		c4.setClassPathFiles(classPathFiles);
		c5.setClassPathFiles(classPathFiles);
		c6.setClassPathFiles(classPathFiles);
		c7.setClassPathFiles(classPathFiles);
		c8.setClassPathFiles(classPathFiles);
		
		
		c1.setParameterValue("-version", null);
		c2.setParameterValue("-version", null);
		//c3.setParameterValue("-version", null);
		c4.setParameterValue("-version", null);
		c5.setParameterValue("-Version", null);
		c6.setParameterValue("-version", null);
		c7.setParameterValue("-version", null);
		c8.setParameterValue("-version", null);
		
		
//		System.out.println("------------------------------------------------------------------------");
//		c1.execute();
//		System.out.println("------------------------------------------------------------------------");
//		c2.execute();
//		System.out.println("------------------------------------------------------------------------");
//		c4.execute();
//		System.out.println("------------------------------------------------------------------------");
//		c5.execute();
		System.out.println("------------------------------------------------------------------------");
		c6.execute();
		System.out.println("------------------------------------------------------------------------");
		c7.execute();
		System.out.println("------------------------------------------------------------------------");
		c8.execute();
		System.out.println("------------------------------------------------------------------------");

	}
}
