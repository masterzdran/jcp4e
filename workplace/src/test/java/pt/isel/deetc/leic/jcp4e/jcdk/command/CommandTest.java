/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.command;


import static org.junit.Assert.assertTrue;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.AllowEmpty;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IsMandatory;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.ValidValues;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;

public class CommandTest {
	private static final String TEST_COMMAND_JAR = "testCommand.jar";
	private static final String TESTING_COMMAND = " testing command ";
	private static final String TEST_COMMAND = "TEST COMMAND";
	private Command c;
	
	private Parameter getNewParamenter(String parameter, String parameterLabel, String parameterValue){
		Parameter p = new Parameter();
		p.setParameterOption(parameter);
		p.setParameterLabel(parameterLabel);
		p.setParameterValue(parameterValue);
		return p;
	}
	
	
	@Before
	public void prepare(){
		c = new Command();
		c.setName(TEST_COMMAND);
		c.setCommandLabel(TESTING_COMMAND);
		c.setCommandEntryPoint(TEST_COMMAND_JAR);
	}
	@Test
	public void commandFieldsTest()
	{
		assertTrue(c.getName().equals(TEST_COMMAND));
		assertTrue(c.getCommandLabel().equals(TESTING_COMMAND));
		assertTrue(c.getCommandEntryPoint().equals(TEST_COMMAND_JAR));
	}
	
	@Test
	public void commandWithOutParametersTest(){
//		System.out.println("2");
//		c.execute();
	}
	@Test
	public void commandWithOneParameterTest(){
		Parameter p = getNewParamenter("-o", "Output","");
		c.addParameter(p);
		//System.out.println("3");
		//c.execute();
	}
	@Test
	public void commandWithMultipleParametersTest(){
		Parameter p1 = getNewParamenter("-out", "Output","");
		c.addParameter(p1);
		Parameter p2 = getNewParamenter("-prt", "print","");
		c.addParameter(p2);
		//c.execute();
	}
	@Test
	public void commandWithMultipleParametersMixedRulesTest()
	{
		Parameter p1 = getNewParamenter("-output", "Output","");
		p1.addParameterRule(new AllowEmpty());
		p1.addParameterRule(new IsMandatory());
		assertTrue(p1.isValid());
		c.addParameter(p1);
		Parameter p2 = getNewParamenter("-prnt", "print","");
		c.addParameter(p2);
		
		//c.execute();
		
	}
	@Test
	public void XMLTest()
	{
		String filename="CommandTEST.XML";
		Parameter p1 = getNewParamenter("-output", "Output","");
		//p1.addParameterRule(new AllowEmpty());
		//p1.addParameterRule(new IsMandatory());
		assertTrue(p1.isValid());
		p1.addParameterValidation(JCDKValidation.HasParameter);
		p1.addParameterValidation(JCDKValidation.AllowEmpty);
		p1.addParameterValidation(JCDKValidation.IsOpcional);
		c.addParameter(p1);
		Parameter p2 = getNewParamenter("-prnt", "print","");
		p2.addParameterRule(new ValidValues("option 1","option 2","option 3","option 4"));
		c.addParameter(p2);
		
		JCP4EXML<Command> xc = new JCP4EXML<Command>();
		xc.showOutput(true);
		
		xc.XMLWrite(c,filename);
		System.out.println("##############################################");
		System.out.println(c.toString());
		
		ICommand command = (ICommand)xc.XMLRead(new Command(),filename);
		if (command != null){
			System.out.println("##############################################");
			System.out.println(command.toString());			
			System.out.println("##############################################");
		}
		System.out.println(c.hashCode());
		System.out.println(command.hashCode());
		Assert.assertTrue(command.equals(c));
	}

}
