/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.parameter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.AllowEmpty;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IParameterRule;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IsMandatory;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IsOptional;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.RequireValue;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.ValidValues;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;


public class ParameterTest {
	Parameter p;
	
	@Before
	public void prepare(){
		p= new Parameter();
	}
	
	private void addParameter(){
		p.setParameterOption("-o");
		p.setParameterLabel("Output");
	}
	@Test
	public void parameterHasNoParamenterTest(){
		assertTrue("Parameter has no parameter", p.isValid());
	}
	@Test
	public void parameterAllowEmptyTest(){
		addParameter();
		p.addParameterRule(new AllowEmpty());
		assertTrue("Parameter has parameter with out Value", p.isValid());		
	}
	@Test
	public void parameterIsOptionalTest(){
		addParameter();
		p.addParameterRule(new IsOptional());
		assertTrue("Parameter has parameter with out Value", p.isValid());			
	}
	@Test
	public void parameterIsMandatoryTest(){
		addParameter();
		p.addParameterRule(new IsMandatory());
		assertFalse("Parameter is Mandatory is not set failed", p.isValid());
		p.setParameterValue("");
		assertTrue("Parameter is Mandatory is set. Success", p.isValid());
	}
	@Test
	public void parameterIsRequiredValueTest(){
		addParameter();
		p.addParameterRule(new RequireValue());
		assertTrue("Parameter has parameter with out Value", p.isValid());
		p.setParameterValue("my output");
		assertTrue("Parameter has parameter with Value", p.isValid());
	}
	@Test
	public void parameterHasValidValuesTest(){
		addParameter();
		p.addParameterRule(new ValidValues("option 1","option 2",null,""));
		assertTrue("Parameter has parameter without Value", p.isValid());
		p.setParameterValue(null);
		assertFalse("Parameter has parameter null Value", p.isValid());
		p.setParameterValue("");
		assertTrue("Parameter has parameter empty Value", p.isValid());
		p.setParameterValue("Option");
		assertTrue("Parameter has parameter invalid Value", p.isValid());
		p.setParameterValue("option 1");
		assertTrue("Parameter has parameter valid Value", p.isValid());
	}	
	@Test
	public void parameterOptionTest()
	{
		addParameter();
		assertTrue(p.getParameterOption().compareTo("-o") == 0);
	}
	@Test
	public void parameterValueTest()
	{
		assertTrue(p.getParameterValue() == null || p.getParameterValue().trim().isEmpty());
	}
	@Test
	public void isParameterSetTest()
	{
		p.setParameterValue("teste");
		assertTrue(p.isSet());
		p.setParameterValue("Test");
		assertTrue(p.isSet());		
		p.setParameterValue("Suprise");
		assertTrue(p.isSet());
		p.setParameterValue("Ja chega");
		assertTrue(p.isSet());
		p.setParameterValue("Must Fail");
		assertTrue(p.isSet());
	}
	@Test
	public void isParameterValid() 
	{
		p.setParameterValue(null);
		assertTrue(p.isValid());
		p.setParameterValue("teste");
		assertTrue(p.isValid());
		p.setParameterValue("Test");
		assertTrue(p.isValid());
		p.setParameterValue("Suprise");
		assertTrue(p.isValid());
		p.setParameterValue("Ja chega");
		assertTrue(p.isValid());
		
		System.out.println(p.toString());
	}
	
	private Parameter getNewParamenter(String parameter, String parameterLabel, String parameterValue){
		Parameter p = new Parameter();
		p.setParameterOption(parameter);
		p.setParameterLabel(parameterLabel);
		p.setParameterValue(parameterValue);
		return p;
	}
	
	
	@Test
	public void xmlTest(){
		String filename="PARAMETERTEST.XML";
		
		Parameter p1 = getNewParamenter("-output", "Output","");
		
		IParameterRule ipr1 = new RequireValue();
		IParameterRule ipr2 = new RequireValue();
		
		assertTrue(ipr1.equals(ipr2));
		p1.addParameterRule(new AllowEmpty());
		p1.addParameterRule(new IsMandatory());
		p1.addParameterRule(new ValidValues("option 1","option 2"));
		p1.addParameterValidation(JCDKValidation.HasParameter);
		p1.addParameterValidation(JCDKValidation.AllowEmpty);
		p1.addParameterValidation(JCDKValidation.IsOpcional);
		System.out.println(p1.isValid());
		System.out.println(p1.toString());
		JCP4EXML<Parameter> iprx = new JCP4EXML<Parameter>();
		iprx.XMLWrite(p1, filename);
		Parameter p2 = (Parameter) iprx.XMLRead(new Parameter(),filename);
		System.out.println("---> "+p2.hashCode() );
		System.out.println("---> "+p1.hashCode());
		Assert.assertTrue(p1.equals(p2));
		
	}

}
