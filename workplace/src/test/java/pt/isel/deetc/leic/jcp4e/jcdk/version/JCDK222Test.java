package pt.isel.deetc.leic.jcp4e.jcdk.version;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pt.isel.deetc.leic.jcp4e.jcdk.profile.ProfileCommands;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;

public class JCDK222Test {
	@Test
	public void test() {
		String filename="CommandProfileTest2.xml";
		ProfileCommands cp = JCDK222.getCommandProfile();
		//this.XMLWrite(cp);
		JCP4EXML<ProfileCommands> x = new JCP4EXML<ProfileCommands>();
		x.showOutput(true);
		x.XMLWrite(cp, filename);
		
		//CommandProfile cpr = this.XMLRead();
		ProfileCommands cpr2 = (ProfileCommands)x.XMLRead(new ProfileCommands(), filename);
		System.out.println(cpr2.hashCode());
		System.out.println(cp.hashCode());
		//assertTrue(cp.equals(cpr));
		assertTrue(cp.equals(cpr2));
	}


}
