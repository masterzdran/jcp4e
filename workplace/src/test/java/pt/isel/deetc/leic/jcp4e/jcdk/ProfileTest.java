/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk;

import junit.framework.Assert;

import org.junit.Test;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKTypes;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profiles;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;

public class ProfileTest {

	private Profile newProfile(String filename,String id)
	{
		Profile p1 = new Profile(filename);
		p1.setBinPath("/bin");
		p1.setFilename(filename);
		p1.setHomePath("/home");
		p1.setId(id);
		p1.setLibPath("/lib");
		p1.setProfileLabel("JCK P1");
		p1.setType(JCDKTypes.Classic);
		p1.setVersion("3.0");
		return p1;
	}
	@Test
	public void test() {
		String filename="ProfilerTEST.XML";
		Profile p1 = newProfile("P1.xml","JCK1");
		Profile p2 = newProfile("P2.xml","JCK2");
		Profile p3 = newProfile("P3.xml","JCK3");
		Profile p4 = newProfile("P4.xml","JCK4");
		p4.setDefaultProfile(true);
		
		Profiles p = new Profiles();
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		
		//Gerar o XML
		JCP4EXML<Profiles> px = new JCP4EXML<Profiles>();
		px.XMLWrite(p, filename);
		
		System.out.println(p.toString());
		System.out.println("-------------------------------------------------------------------");
		//Ler o XML
		
		Profiles ap =(Profiles) px.XMLRead(new Profiles(), filename);
		
		System.out.println(ap.toString());
		
		System.out.println("-------------------------------------------------------------------");
		
		System.out.println(p.hashCode());
		System.out.println(ap.hashCode());

		
		Assert.assertTrue(p.equals(ap));
	}

}
