/**
 * 
 */
package pt.isel.deetc.leic.jcp4e.jcdk;

import java.util.Set;


import org.junit.Assert;
import org.junit.Test;

import pt.isel.deetc.leic.jcp4e.jcdk.command.IGuiCommand;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;

/**
 * @author masterzdran
 *
 */
public class JCDKManagerTest {

	@Test
	public void test() {
		String PathToEclipse = "/home/masterzdran/Applications/eclipse/configuration/pt.isel.deetc.leic.jcp4e";
		
		//Create a instance of manager
		JCDKManager manager = new JCDKManager(PathToEclipse);
	
		//Get and Select an profile
		IProfile p = null;
		for(IProfile t : manager.getAllProfiles()){
			if(t.getProfileLabel().trim().equals("n3")){
				p = t; break;
			}
		}
		
		//Assert that p is not null and has value
		Assert.assertTrue(p != null);
		Set<IGuiCommand> c= null ;
		//if load commands went well load the commands
		if (manager.loadCommands(p)){
			c=manager.getAllCommands();
		}
		
		//Assert that c is not null and has value
		Assert.assertTrue(c!= null);
		if(c!= null){
			//Assert that the size of the set is 8
			Assert.assertTrue(c.size() == 8);
			System.out.println("---->>" + c.size() + "<<----");
			
			for(IGuiCommand g : c){
				System.out.println(g.getName());
			}
			
		}
	}

}
