/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.util;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 * @param <T>
 * 
 * Generic Class to provide support to writing and reading XML (JAXB) files.
 */
public class JCP4EXML<T> {

	private boolean _show;

	/**
	 * This method provides the ability of writing XML file (using the JAXB technology) from the type T.
	 * @param type
	 * @param filename
	 * @return
	 */
	public boolean XMLWrite(T type, String filename)
	{
		try {

			File file = new File(filename);
			JAXBContext jaxbContext = JAXBContext.newInstance(type.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(type, file);
			if(_show)
				jaxbMarshaller.marshal(type, System.out);
		} catch (JAXBException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * This method provides the ability of reading an XML file (using the JAXB technology) to the type T.
	 * @param type
	 * @param filename
	 * @return
	 */
	public Object  XMLRead(T type, String filename)
	{
		
		try {
			File file = new File(filename);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(type.getClass());
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			return jaxbUnmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * This method set the option show to be visible in the console the output of the XML generation.
	 * @param show
	 */
	public void showOutput(boolean show){
		_show = show;
	}
}
