/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk;

import java.io.File;
import java.util.Set;

import pt.isel.deetc.leic.jcp4e.jcdk.command.IGuiCommand;
import pt.isel.deetc.leic.jcp4e.jcdk.manager.Manager;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.ProfileCommands;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profiles;

/**
 * 
 * This should be the entry point of the communication between Gui (Eclipse) and command architecture   
 *
 */
public class JCDKManager {
	private final String _profilesFile="JCDK.Profiles.xml";
	private final String _configBasePath;
	private Manager<IProfile, Profiles> _profileManager;
	private Manager<IGuiCommand, ProfileCommands> _profileCommandsManager;
	
	/**
	 * Class Constructor.
	 * path should be absolute path to the eclipse plugin folder.
	 * @param path
	 */
	public JCDKManager(String path) {
		_configBasePath = path;
		_profileManager = new Manager<IProfile, Profiles>(new Profiles(), _configBasePath+File.separatorChar+_profilesFile);
		_profileManager.load();
	}
	/**
	 * Save the profiles structure
	 * @return
	 */
	public boolean saveProfiles()
	{
		return _profileManager.save();
	}
	/**
	 * Get an collection of profiles
	 * @return
	 */
	public Set<IProfile> getAllProfiles(){
		return _profileManager.getAll();
	}
	/**
	 * Add a new profile
	 * @param profile
	 * @return
	 */
	public boolean addProfile(IProfile profile)
	{
		if (profile == null) return false;
		return _profileManager.add(profile);
	}

	/**
	 * Remove an profile
	 * @param profile
	 * @return
	 */
	public boolean removeProfile(IProfile profile)
	{
		if (profile == null) return false;
		return _profileManager.remove(profile);
	}
	/**
	 * Set a profile
	 * @param profile
	 * @return
	 */
	public boolean setProfile(IProfile profile)
	{
		if (profile == null) return false;
		return _profileManager.set(profile);
	}
	/**
	 * Set an collection of Commands
	 * @return
	 */
	public Set<IGuiCommand> getAllCommands(){
		if (_profileCommandsManager == null) return null;
		return _profileCommandsManager.getAll();
	}
	/**
	 * Save the command structure
	 * @return
	 */
	public boolean saveCommands()
	{
		if(_profileCommandsManager == null) return false;
		return _profileCommandsManager.save();
	}
	/**
	 * Load the command structure
	 * @param profile
	 * @return
	 */
	public boolean loadCommands(IProfile profile)
	{
		if (profile == null) return false;
		_profileCommandsManager = new Manager<IGuiCommand, ProfileCommands>(new ProfileCommands(), _configBasePath+'/'+profile.getFilename());
		return _profileCommandsManager.load();
	}

	/**
	 * Add a Command
	 * @param command
	 * @return
	 */
	public boolean addCommand(IGuiCommand command)
	{
		if(_profileCommandsManager == null || command == null) return false;
		return _profileCommandsManager.add(command);
	}
	/**
	 * Remove an command
	 * @param command
	 * @return
	 */
	public boolean removeCommand(IGuiCommand command)
	{
		if(_profileCommandsManager == null || command == null) return false;
		return _profileCommandsManager.remove(command);
	}	
	/**
	 * Set an command
	 * @param command
	 * @return
	 */
	public boolean setCommand(IGuiCommand command)
	{
		if(_profileCommandsManager == null || command == null) return false;
		return _profileCommandsManager.set(command);
	}
}
