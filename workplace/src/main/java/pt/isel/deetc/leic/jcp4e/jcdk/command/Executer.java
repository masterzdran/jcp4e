/**	
 * This file is part of JCP4E - Java Card Plugin for Eclipse.
 * 
 * JCP4E is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JCP4E is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * Executer of the command
 *
 */
public class Executer {
	
	private BufferedReader _outputInputStreamReader;
	private BufferedReader _errorInputStreamReader;
	
	/**
	 * Execute the parameter
	 * @param execute
	 */
	public void executer(StringBuilder execute) {
		try {
			System.out.println(execute.toString());
			Process proc =  Runtime.getRuntime().exec(execute.toString());
			/**
			 * According to Process.getInputStream() documentation:
		     * Returns the input stream connected to the normal output of the
		     * subprocess.  The stream obtains data piped from the standard
		     * output of the process represented by this {@code Process} object.
		     *
		     * <p>If the standard output of the subprocess has been redirected using
		     * {@link ProcessBuilder#redirectOutput(Redirect)
		     * ProcessBuilder.redirectOutput}
		     * then this method will return a
		     * <a href="ProcessBuilder.html#redirect-output">null input stream</a>.
		     *
		     * <p>Otherwise, if the standard error of the subprocess has been
		     * redirected using
		     * {@link ProcessBuilder#redirectErrorStream(boolean)
		     * ProcessBuilder.redirectErrorStream}
		     * then the input stream returned by this method will receive the
		     * merged standard output and the standard error of the subprocess.
		     *
		     * <p>Implementation note: It is a good idea for the returned
		     * input stream to be buffered.
		     *
		     * @return the input stream connected to the normal output of the
		     *         subprocess
		     */
			_outputInputStreamReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			_errorInputStreamReader = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	


	/**
	 * Get the STDOUT of the execution
	 * @return
	 */
	public BufferedReader getOutputStreamReader() {
		return _outputInputStreamReader;
	}

	/**
	 * Get the STDERR of the execution 
	 * @return
	 */
	public BufferedReader getErrorStreamReader() {
		return _errorInputStreamReader;
	}
	/**
	 * Clear the STDOUT/STDERR stream
	 */
	public void clearStream(){
		_errorInputStreamReader = _outputInputStreamReader = null;
	}
	

}
