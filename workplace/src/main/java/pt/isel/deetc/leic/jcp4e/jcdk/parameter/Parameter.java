/**	
 * This file is part of JCP4E - Java Card Plugin for Eclipse.
 * 
 * JCP4E is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JCP4E is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 *
 */
package pt.isel.deetc.leic.jcp4e.jcdk.parameter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IParameterRule;

/**
 * 
 * This class represent one and only one parameter of an command.
 *
 */
@XmlRootElement
public class Parameter implements IGuiParameter {
	private Set<IParameterRule> parameterRules;
	private String parameterOption;
	private String parameterLabel;
	private String parameterValue;
	private Set<JCDKValidation> validations;
	private boolean _isSet;
	
	/**
	 * 
	 * @param name : name of the parameter.
	 * @param value: value of the parameter
	 * @param description: description of the parameter
	 * @param rules: collection of rules, this parameter must meet.
	 */
	public Parameter(String name, String value, String description,Iterable<IParameterRule> rules) {
		parameterOption = name;
		parameterLabel = description;
		parameterValue = value;
		parameterRules = new HashSet<IParameterRule>();
		_isSet = false;
		validations = new HashSet<JCDKValidation>();
		for(IParameterRule r : rules)
		{
			parameterRules.add(r);
		}
	}

	/**
	 * 
	 * @param name : name of the parameter.
	 * @param description: description of the parameter
	 * @param rules: collection of rules, this parameter must meet.
	 */
	public Parameter(String name, String description,Iterable<IParameterRule> rules) {
		this(name, null, description, rules);
	}
	/**
	 * 
	 * @param name : name of the parameter.
	 * @param value: value of the parameter
	 * @param description: description of the parameter
	 */
	public Parameter(String name, String value, String description) {
		this(name, value, description, new ArrayList<IParameterRule>());
	}
	/**
	 * 
	 * @param name : name of the parameter.
	 * @param value: value of the parameter
	 */
	public Parameter(String name, String value) {
		this(name, value, null, new ArrayList<IParameterRule>());
	}
	/**
	 * this constructor is need by the JAXB
	 */
	public Parameter() {
		this(null, null, null, new ArrayList<IParameterRule>());
	}

	/**
	 * Add an type of validation so it could be used by the graphic interface to generate the GUI.
	 * @param validation
	 * 
	 */
	public void addParameterValidation(JCDKValidation validation) {
		validations.add(validation);
	}
	/**
	 * Remove an Validation.
	 * NOTE: Do not confuse an Validation with an parameter rule. This Enum should only be used as reference by eclipse GUI.
	 * @param validation
	 */
	public void removeParameterValidation(JCDKValidation validation) {
		if (validations.contains(validation))
				validations.remove(validation);
		
	}
	/**
	 * Add an collection of parameter rules.
	 * @param parameterRules
	 */
	public void addRules(List<IParameterRule> parameterRules) {
		for (IParameterRule p : parameterRules)
			this.parameterRules.add(p);
	}

	/**
	 * Add one parameter rule
	 * @param parameterRule
	 */
	public void addParameterRule(IParameterRule parameterRule) {
		parameterRules.add(parameterRule);
	}
	/**
	 * remove one parameter rule
	 * @param parameterRule
	 */
	public void removeParameterRule(IParameterRule parameterRule) {
		if(parameterRules.contains(parameterRule))
			parameterRules.remove(parameterRule);
	}

	/**
	 * return the parameter name.
	 */
	@Override
	@XmlAttribute
	public String getParameterOption() {
		return this.parameterOption;
	}
	/**
	 * Set the parameter name
	 * @param parameter
	 */
	public void setParameterOption(String parameter) {
		this.parameterOption = parameter;
	}


	/**
	 * Get the option value
	 * @return
	 */
	@Override
	@XmlAttribute
	public String getParameterValue() {
		return parameterValue;
	}
	/**
	 * Set the option value
	 * @param parameterValue
	 */
	public void setParameterValue(String parameterValue) {
		String pv = this.parameterValue;
		this.parameterValue = parameterValue;
		_isSet = true;
		if (parameterRules == null)
			return;
		// verify if parameter is still valid
		if (!isValid()) {
			this.parameterValue = pv;
		}
	}

	/**
	 * 
	 * Get parameter label.
	 * Note: This label could be use as the eclipse gui Label.
	 * @return
	 */
	@Override
	@XmlAttribute
	public String getParameterLabel() {
		return parameterLabel;
	}
	/**
	 * 
	 * Set parameter label.
	 * Note: This label could be use as the eclipse gui Label.
	 */
	public void setParameterLabel(String parameterLabel) {
		this.parameterLabel = parameterLabel;
	}

	
	/**
	 * Get an collection of validations.
	 */
	@Override
	@XmlElementWrapper(name = "validations")
	@XmlElement(name = "validation")
	public Set<JCDKValidation> getValidations() {
		return validations;
	}
	/**
	 * Set an collection of validations
	 * @param validations
	 */
	public void setValidations(Set<JCDKValidation> validations) {
		this.validations = validations;
	}

	
	/**
	 * Get an collection of rules
	 * @return
	 */
	@XmlAnyElement
	@XmlElementWrapper(name="ParameterRules")
	// It's necessary so it could be possible JAXB handle Interfaces
	@XmlElementRefs({
			@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.AllowEmpty.class),
			@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IsMandatory.class),
			@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IsOptional.class),
			@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.RequireValue.class),
			@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.RequireNoValue.class),
			@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.ValidValues.class) 
	})
	public Set<IParameterRule> getParameterRules() {
		return parameterRules;
	}

	/**
	 * Set an collection of rules
	 * @param parameterRules
	 */
	public void setParameterRules(Set<IParameterRule> parameterRules) {
		this.parameterRules = parameterRules;
	}
	
	/**
	 * Clear parameter value.
	 */
	@Override
	public void clear() {
		setParameterValue(null);
		_isSet = false;
	}

	/**
	 * Verify if parameter has been set.
	 */
	@Override
	public boolean isSet() {
		return _isSet;
	}

	/**
	 * Verify if parameter is valid
	 */
	@Override
	public boolean isValid() {
		for (IParameterRule r : parameterRules) {
			if (!r.isValid(this))
				return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if (isSet()) {
			s.append(parameterOption);
			if (!(parameterValue == null || parameterValue.trim().isEmpty()))
				s.append(" " + parameterValue);
		}
		return s.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((parameterLabel == null) ? 0 : parameterLabel.hashCode());
		result = prime * result
				+ ((parameterOption == null) ? 0 : parameterOption.hashCode());
		result = prime * result
				+ ((parameterRules == null) ? 0 : parameterRules.hashCode());
		result = prime * result
				+ ((parameterValue == null) ? 0 : parameterValue.hashCode());
		result = prime * result
				+ ((validations == null) ? 0 : validations.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parameter other = (Parameter) obj;
		if (parameterLabel == null) {
			if (other.parameterLabel != null)
				return false;
		} else if (!parameterLabel.equals(other.parameterLabel))
			return false;
		if (parameterOption == null) {
			if (other.parameterOption != null)
				return false;
		} else if (!parameterOption.equals(other.parameterOption))
			return false;
		if (parameterRules == null) {
			if (other.parameterRules != null)
				return false;
		} else if (!parameterRules.equals(other.parameterRules))
			return false;
		if (parameterValue == null) {
			if (other.parameterValue != null)
				return false;
		} else if (!parameterValue.equals(other.parameterValue))
			return false;
		if (validations == null) {
			if (other.validations != null)
				return false;
		} else if (!validations.equals(other.validations))
			return false;
		return true;
	}
	
}
