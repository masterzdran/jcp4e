/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.manager;

import java.util.Set;
/**
 * 
 * Provide an generic interface of Manager instance.
 * Eclipse GUI should talk to this interface for better performance and simplicity.
 *
 * @param <T>
 */
public interface IManager<T> {

	public abstract boolean load();

	public abstract boolean save();

	public abstract boolean add(T object);

	public abstract boolean remove(T object);

	public abstract boolean set(T object);

	public abstract Set<T> getAll();

}