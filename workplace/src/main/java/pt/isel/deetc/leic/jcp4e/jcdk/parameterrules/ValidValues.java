/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.parameterrules;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.IParameter;

/**
 * 
 * class Valid Values. This class represents a rule tha set a collection of valid, admissible values from the parameter
 *
 */
@XmlRootElement
public class ValidValues implements IParameterRule {
	
	@XmlElement
	private Set<String> validvalues;
	private final JCDKValidation _type;

	public ValidValues() {validvalues = new HashSet<String>(); _type=JCDKValidation.ValidValues;}
	public ValidValues(String... value) {
		validvalues = new HashSet<String>();
		for (String val : value) {
			if (!(val == null || val.trim().isEmpty()))
				validvalues.add(val);
		}
		_type=JCDKValidation.ValidValues;
	}
	/**
	 * Get the type of the validation
	 */
	@Override
	public JCDKValidation getType() {
		return _type;
	}
	/**
	 * return if the parameter respect the rule
	 */
	@Override
	public boolean isValid(IParameter parameter) {
		if (!parameter.isSet()) return true;
		String p = parameter.getParameterValue();
		if (p == null )	return false;
		if( p.trim().isEmpty() ) return true;

		boolean exit = false;
		for ( String value : validvalues)
			if (p.equals(value))
				exit = true;
		return exit;
	}
	/**
	 * Set the collection of valid values. 
	 * NOTE: This is useful because JAXB.
	 * @param validvalues
	 */
	public void setValidvalues(Set<String> validvalues) {
		this.validvalues = validvalues;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((validvalues == null) ? 0 : validvalues.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ValidValues)) {
			return false;
		}
		ValidValues other = (ValidValues) obj;
		if (validvalues == null) {
			if (other.validvalues != null) {
				return false;
			}
		} else if (!validvalues.equals(other.validvalues)) {
			return false;
		}
		return true;
	}

}
