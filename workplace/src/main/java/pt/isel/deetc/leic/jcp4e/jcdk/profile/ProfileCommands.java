	/**	
	* This file is part of JCP4E - Java Card Plugin for Eclipse.
	* 
    * JCP4E is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
	*
    * JCP4E is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
	* 
    * You should have received a copy of the GNU General Public License
    * along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
	*/

/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 *
 */
package pt.isel.deetc.leic.jcp4e.jcdk.profile;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;
import pt.isel.deetc.leic.jcp4e.jcdk.command.IGuiCommand;

/**
 * 
 * Represent an collection of commands, from a specific JCDK profile
 *
 */
@XmlRootElement
public class ProfileCommands implements ICollection<IGuiCommand> {
	
	private HashMap<String, IGuiCommand> commands;
	
	
	public ProfileCommands() {
		commands=new HashMap<String, IGuiCommand>();
	}
	
	/**
	 * Add a new Command. Command is an instance of IGuiCommand and is useful to eclipse gui interface.
	 * @param command
	 * @return
	 */
	public boolean add(IGuiCommand command)
	{
		if (command == null) return false;
		IGuiCommand c = commands.put(((Command)command).getName(), command);
		return c != null;
	}
	
	/**
	 * Remove a specific Command. Command is an instance of IGuiCommand and is useful to eclipse gui interface.
	 * @param command
	 * @return
	 */
	public boolean remove(IGuiCommand command)
	{
		if (command == null) return false;
		IGuiCommand c = commands.get(((Command)command).getName());
		if (c!= null)
			commands.remove(c.getName());
		return c != null;
	}

	/**
	 * Get a collection of Command. Command is an instance of IGuiCommand and is useful to eclipse gui interface.
	 * @return
	 */
	@XmlAnyElement
	@XmlElementWrapper(name = "commands")
	@XmlElementRefs({
		@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.command.Command.class)})
	public Set<IGuiCommand> getCommands() {
		HashSet<IGuiCommand> prmtr = new HashSet<IGuiCommand>();
		for (String s : commands.keySet()) {
			Command c =(Command) commands.get(s);
			prmtr.add(c);
		}
		return prmtr;
	}

	/**
	 * Set an collection of Command. Command is an instance of IGuiCommand and is useful to eclipse gui interface.
	 * @param commands
	 */
	public void setCommands(Set<IGuiCommand> commands) {
		for (IGuiCommand c : commands) {
			this.add(c);
		}
	}

	@Override
	public boolean set(IGuiCommand command) {
		return this.add(command);
	}


	@Override
	public Set<IGuiCommand> getAll() {
		return this.getCommands();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commands == null) ? 0 : commands.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ProfileCommands)) {
			return false;
		}
		ProfileCommands other = (ProfileCommands) obj;
		if (commands == null) {
			if (other.commands != null) {
				return false;
			}
		} else if (!commands.equals(other.commands)) {
			return false;
		}
		return true;
	}



}
