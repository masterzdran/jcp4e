/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.manager;

import java.io.File;
import java.util.Set;

import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profiles;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;
@Deprecated
public class ProfileManager implements IManager<IProfile> {
	
	private final String 	_filename;
	private Profiles 	_profiles;
	private boolean _changed;
	
	public ProfileManager(String filename) {
		_filename = filename;
		_profiles = new Profiles();
	}
	/**
	 * Load the XML file if he exist
	 */
	@Override
	public boolean load()
	{
		if (_filename == null) return false;
		File f = new File(_filename);
		
		if (!f.exists())
			return false;
		
		
		JCP4EXML<Profiles> px = new JCP4EXML<Profiles>();
		Profiles dummie = new Profiles();
		Profiles ap =(Profiles) px.XMLRead(dummie, _filename);
		if (ap == null) return false;
		_profiles = ap;
		return true;		
	}
	/**
	 * Save the XML file
	 */
	@Override
	public boolean save()
	{
		JCP4EXML<Profiles> px = new JCP4EXML<Profiles>();
		boolean r = px.XMLWrite(_profiles, _filename); 
		if (r) _changed = false;
		return r;
	}
	/**
	 * Add a new Command
	 */
	@Override
	public boolean add(IProfile profile)
	{
		if (_profiles == null || profile == null) return false;
		boolean r = _profiles.add(profile);
		if (r) _changed = true;
		return  r;
	}
	/**
	 * remove a Command
	 */
	@Override
	public boolean remove(IProfile profile)
	{
		if (_profiles == null || profile == null) return false;
		boolean r = _profiles.remove(profile);
		if (r) _changed = true;
		return  r;
	}
	/**
	 * Set the command
	 */
	@Override
	public boolean set(IProfile profile)
	{
		return this.add(profile);
	}
	/**
	 * Retorn all commands
	 */
	@Override
	public Set<IProfile> getAll()
	{
		if (_profiles == null ) return null;
		return  _profiles.getProfiles();
	}
	/**
	 * This action save the xml file, if the profile has been changed
	 */
	@Override
	protected void finalize() throws Throwable {
		if(_changed)
			this.save();		
		super.finalize();
	}
	
	
}
