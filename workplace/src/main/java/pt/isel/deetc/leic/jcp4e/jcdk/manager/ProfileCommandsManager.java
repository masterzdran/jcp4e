/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.manager;

import java.io.File;
import java.util.Set;

import pt.isel.deetc.leic.jcp4e.jcdk.command.IGuiCommand;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.ProfileCommands;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;

/**
 * 
 * This class Manage the Profile Command instances
 *
 */
@Deprecated
public class ProfileCommandsManager implements IManager<IGuiCommand>{
	
	private final String _filename;
	private ProfileCommands _commands;
	private boolean _changed;
	
	public ProfileCommandsManager(String filename) {
		_filename = filename;
		_commands = new ProfileCommands();
	}

	/**
	 * Load the XML file if he exist
	 */
	@Override
	public boolean load() {
		File f = new File(_filename);
		if (!f.exists())
			return false;

		if (_filename == null) return false;
		JCP4EXML<ProfileCommands> px = new JCP4EXML<ProfileCommands>();
		ProfileCommands dummie = new ProfileCommands();
		ProfileCommands ap =(ProfileCommands) px.XMLRead(dummie, _filename);
		if (ap == null) return false;
		_commands = ap;
		return true;	
	}

	/**
	 * Save the XML file
	 */
	@Override
	public boolean save() {
		JCP4EXML<ProfileCommands> px = new JCP4EXML<ProfileCommands>();
		boolean r = px.XMLWrite(_commands, _filename);
		if (r) _changed = false;
		return r;
	}

	/**
	 * Add a new Command
	 */
	@Override
	public boolean add(IGuiCommand command) {
		if ( command == null ) return false;
		boolean r =_commands.add(command);
		if (r) _changed = true;
		return r;
	}
	/**
	 * remove a Command
	 */
	@Override
	public boolean remove(IGuiCommand command) {
		if ( command == null ) return false;
		boolean r = _commands.remove(command);
		if (r) _changed = true;
		return r;
	}

	/**
	 * Set the command
	 */
	@Override
	public boolean set(IGuiCommand command) {
		return this.add(command);
	}

	/**
	 * Retorn all commands
	 */
	@Override
	public Set<IGuiCommand> getAll() {
		return _commands.getCommands();
	}

	/**
	 * This action save the xml file, if the profile has been changed
	 */
	@Override
	protected void finalize() throws Throwable {
		if(_changed)
			this.save();	
		super.finalize();
	}

}
