/**	
 * This file is part of JCP4E - Java Card Plugin for Eclipse.
 * 
 * JCP4E is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JCP4E is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 *
 */
package pt.isel.deetc.leic.jcp4e.jcdk.profile;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * This class represent a set of JCDK profiles.
 *
 */
@XmlRootElement
public class Profiles implements ICollection<IProfile>{
	private HashMap<String, IProfile> profiles;

	public Profiles() {
		profiles = new HashMap<String, IProfile>();
	}

	/**
	 * Add a new profile.
	 * @param profile
	 * @return
	 */
	public boolean add(IProfile profile) {
		IProfile p = profiles.put(profile.getId(), profile);
		return p != null;
	}
	
	/**
	 * Remove an profile
	 * @param profile
	 * @return
	 */
	public boolean remove(IProfile profile) {
		if (profile == null)
			return false;
		IProfile p = profiles.get(profile.getId());
		if (p != null)
			p = profiles.remove(p.getId());
		
		return p != null;
	}

	/**
	 * Return an specific profile
	 * @param profile
	 * @return
	 */
	public IProfile getProfile(IProfile profile) {
		if (profile == null)
			return null;
		return profiles.get(profile.getId());
	}

	/**
	 * Returns all available profiles
	 * @return
	 */
	@XmlAnyElement
	@XmlElementWrapper(name = "profiles")
	@XmlElementRefs({
		@XmlElementRef(type = pt.isel.deetc.leic.jcp4e.jcdk.profile.Profile.class)})
	public Set<IProfile> getProfiles() {
		HashSet<IProfile> prmtr = new HashSet<IProfile>();
		for (String s : profiles.keySet()) {
			Profile p =(Profile) profiles.get(s);
			prmtr.add(p);
		}
		return prmtr;
	}

	/**
	 * Sets a collection of profiles
	 * @param profiles
	 */
	public void setProfiles(Set<IProfile> profiles) {

		for (IProfile p : profiles) {
			this.add((Profile)p);
		}
	}
	@Override
	public boolean set(IProfile profile) {
		return this.add(profile);
	}

	@Override
	public Set<IProfile> getAll() {
		return this.getProfiles();
	}
	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		for(String s : profiles.keySet())
		{
			str.append(s+" ");
		}
		str.append("\n");
		return str.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((profiles == null) ? 0 : profiles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Profiles)) {
			return false;
		}
		Profiles other = (Profiles) obj;
		if (profiles == null) {
			if (other.profiles != null) {
				return false;
			}
		} else if (!profiles.equals(other.profiles)) {
			return false;
		}
		return true;
	}



}