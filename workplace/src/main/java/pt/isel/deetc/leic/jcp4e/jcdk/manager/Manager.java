	/**	
	* This file is part of JCP4E - Java Card Plugin for Eclipse.
	* 
    * JCP4E is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
	*
    * JCP4E is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
	* 
    * You should have received a copy of the GNU General Public License
    * along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
	*/

package pt.isel.deetc.leic.jcp4e.jcdk.manager;

import java.io.File;
import java.util.Set;

import pt.isel.deetc.leic.jcp4e.jcdk.profile.ICollection;
import pt.isel.deetc.leic.jcp4e.jcdk.util.JCP4EXML;

/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 *
 */
public class Manager<T,C extends ICollection<T>> implements IManager<T> {
	private final String _filename;
	private C _collection;
	private boolean _changed;
	
	
	public Manager(C instace, String filename) {
		_filename = filename;
		_collection = instace;
	}
	
	@Override
	public boolean load() {
		if (_filename == null) return false;
		File f = new File(_filename);
		
		if (!f.exists())
			return false;
		
		
		JCP4EXML<C> px = new JCP4EXML<C>();
		C dummie = _collection;
		
		@SuppressWarnings("unchecked")
		C ap =(C) px.XMLRead(dummie, _filename);
		if (ap == null) return false;
		_collection = ap;
		return true;
	}

	@Override
	public boolean save() {
		JCP4EXML<C> px = new JCP4EXML<C>();
		boolean r = px.XMLWrite(_collection, _filename);
		if (r) _changed = false;
		return r;
	}
	@Override
	public boolean add(T object) {
		if ( object == null ) return false;
		boolean r =_collection.add(object);
		if (r) _changed = true;
		return r;
	}

	@Override
	public boolean remove(T object) {
		if ( object == null ) return false;
		boolean r = _collection.remove(object);
		if (r) _changed = true;
		return r;
	}

	@Override
	public boolean set(T object) {
		return this.add(object);
	}

	@Override
	public Set<T> getAll() {
		return _collection.getAll();
	}

	@Override
	protected void finalize() throws Throwable {
		if(_changed)
			this.save();	
		super.finalize();
	}
}
