/**	
 * This file is part of JCP4E - Java Card Plugin for Eclipse.
 * 
 * JCP4E is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JCP4E is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;


/**
 * 
 * Represent an Command, with all needed methods
 *
 */
@XmlRootElement
public class Command implements IGuiCommand {

	private HashMap<String, Parameter> parameters;
	private String name;
	private String commandLabel;
	private String commandEntryPoint;

	
	private String _inputFile;
	private String _outputFile;
	private BufferedReader _errorInputStreamReader;
	private BufferedReader _inputInputStreamReader;
	private String _classPathFiles;

	public Command() {
		this(null, null, null);
	}

	/**
	 * 
	 * @param name : Name of the Command
	 * @param commandLabel : Name to be used as eclipse label
	 */
	public Command(String name, String commandLabel) {
		this(name, commandLabel, null);
	}

	/**
	 * 
	 * @param name : Name of the Command
	 * @param commandLabel : Name to be used as eclipse label
	 * @param commandEntryPoint : Java entry point of the command
	 */
	public Command(String name, String commandLabel, String commandEntryPoint) {
		this.name = name;
		this.commandEntryPoint = commandEntryPoint;
		this.commandLabel = commandLabel;
		this.parameters = new HashMap<String, Parameter>();
		this._inputFile = null;
		this._outputFile = null;

	}
	/**
	 * Set the @param classPathFiles represents the classpath libraries to be include when the command is executed
	 * @param classPathFiles
	 */
	public void setClassPathFiles(String classPathFiles) {
		this._classPathFiles = classPathFiles;
	}
	/**
	 * Get the @param classPathFiles represents the classpath libraries to be include when the command is executed 
	 * @return
	 */
	public String getClassPathFiles() {
		
		return (_classPathFiles != null) ? _classPathFiles : "";
	}
	/**
	 * Add a parameter to Command
	 * @param parameter
	 */
	public void addParameter(Parameter parameter) {
		parameters.put(parameter.getParameterOption(), parameter);
	}

	/**
	 * Remove parameter from the command
	 * @param parameter
	 */
	public void removeParameter(String parameter) {
		if (parameters.get(parameter) != null)
			parameters.remove(parameter);
	}

	public void setParameterValue(String option, String value) {
		Parameter p = (Parameter) parameters.get(option);
		if (p != null)
			p.setParameterValue(value);
	}

	/**
	 * Get the Command Name
	 * @return
	 */
	@Override
	@XmlAttribute
	public String getName() {
		return name;
	}

	/**
	 * Set the Command Name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the command label
	 * @return
	 */
	@Override
	@XmlAttribute
	public String getCommandLabel() {
		return commandLabel;
	}
	/**
	 * Set the Command Label
	 * @param commandLabel
	 */
	public void setCommandLabel(String commandLabel) {
		this.commandLabel = commandLabel;
	}

	/**
	 * Get the java entry point of the command
	 * @return
	 */
	@Override
	@XmlAttribute
	public String getCommandEntryPoint() {
		return commandEntryPoint;
	}

	/**
	 * Set the java entry point of the command
	 * @param commandEntryPoint
	 */
	public void setCommandEntryPoint(String commandEntryPoint) {
		this.commandEntryPoint = commandEntryPoint;
	}

	/**
	 * Print the STDOUT of the command
	 */
	public void printStandardOutput(){
		printStandard(_inputInputStreamReader);
	}
	/**
	 * Print the STDERR of the command
	 */
	public void printStandardError(){
		printStandard(_errorInputStreamReader);
	}
	
	private void printStandard(BufferedReader input){
		String line;
		try {
			while((line = input.readLine()) != null){
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Execute the command
	 */
	@Override
	public void execute() {
		if (evaluateParameters()) {
			//prepare execution string
			StringBuilder s = new StringBuilder();
			s.append("java ");
			s.append(this.getClassPathFiles()+' ');
			s.append(this.toString());
			
			//clear previous output
			this.clearOutput();
			
			//execute the command
			executer(s);
			
			//print the outputs
			this.printStandardOutput();
			this.printStandardError();

		}
		//clear the command parameters
		clearParameterValues();
	}

	private void executer(StringBuilder execute) {
		Executer exec = new Executer();
		exec.executer(execute);
		_inputInputStreamReader = exec.getOutputStreamReader();
		_errorInputStreamReader = exec.getErrorStreamReader();
		//exec.clearStream();
	}
	/**
	 * Get STDOUT Stream to be used in eclipse gui console. 
	 * @return
	 */
	public BufferedReader getCommandOutput() {
		return _inputInputStreamReader;
	}
	
	/**
	 * Get STDERR Stream to be used in eclipse gui console.
	 * @return
	 */
	public BufferedReader getCommandErrorOutput() {
		return _errorInputStreamReader;
	}
	
	/**
	 * Clear STDOUT/STDERR Streams
	 */
	public void clearOutput(){
		_inputInputStreamReader = null;
		_errorInputStreamReader = null;
	}

	/**
	 * Get an collection of parameters
	 * @return
	 * 
	 */
	@Override
	@XmlElementWrapper(name = "parameters")
	@XmlElement(name = "parameter")
	public Set<Parameter> getParameters() {
		HashSet<Parameter> prmtr = new HashSet<Parameter>();
		for (String s : parameters.keySet()) {
			Parameter p = (Parameter) parameters.get(s);
			prmtr.add(p);
		}
		return prmtr;
	}

	/**
	 * Set an collection of parameters
	 * @param parameters
	 */
	public void setParameters(Set<Parameter> parameters) {

		for (Parameter p : parameters) {
			this.parameters.put(p.getParameterOption(), p);
		}
	}

	
	
	/**
	 * @return the _inputFile
	 */
	public String get_inputFile() {
		return _inputFile;
	}

	/**
	 * @param inputFile the _inputFile to set
	 */
	public void set_inputFile(String inputFile) {
		this._inputFile = inputFile;
	}

	/**
	 * @return the _outputFile
	 */
	public String get_outputFile() {
		return _outputFile;
	}

	/**
	 * @param outputFile the _outputFile to set
	 */
	public void set_outputFile(String outputFile) {
		this._outputFile = outputFile;
	}

	private void clearParameterValues() {
		Parameter p;
		for (String name : parameters.keySet()) {
			p = (Parameter) parameters.get(name);
			if (p.isSet())
				p.clear();
		}
		_inputFile = null;
		_outputFile = null;
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append(this.commandEntryPoint);
		Parameter p;
		for (String name : parameters.keySet()) {
			p = (Parameter) parameters.get(name);
			if (isValid(p)) {
				if (p.isSet())
					s.append(" " + p.toString());
			}
		}
		if (_inputFile != null) {
			s.append(" " + _inputFile);
			if (_outputFile != null)
				s.append(" " + _outputFile);
		}
		return s.toString();
	}


	private boolean isValid(Parameter p) {
		return p.isValid();
	}

	private boolean evaluateParameters() {
		Parameter p;
		for (String name : parameters.keySet()) {
			p = (Parameter) parameters.get(name);
			if (!isValid(p))
				return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((commandEntryPoint == null) ? 0 : commandEntryPoint
						.hashCode());
		result = prime * result
				+ ((commandLabel == null) ? 0 : commandLabel.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((parameters == null) ? 0 : parameters.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Command other = (Command) obj;
		if (commandEntryPoint == null) {
			if (other.commandEntryPoint != null)
				return false;
		} else if (!commandEntryPoint.equals(other.commandEntryPoint))
			return false;
		if (commandLabel == null) {
			if (other.commandLabel != null)
				return false;
		} else if (!commandLabel.equals(other.commandLabel))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parameters == null) {
			if (other.parameters != null)
				return false;
		} else if (!parameters.equals(other.parameters))
			return false;
		return true;
	}

}
