/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */
package pt.isel.deetc.leic.jcp4e.jcdk.parameterrules;

import javax.xml.bind.annotation.XmlRootElement;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.IParameter;

@XmlRootElement
public class RequireValue implements IParameterRule {
	
	private final JCDKValidation _type;
	public RequireValue(){
		super();
		_type = JCDKValidation.RequireValue;
	}
	/**
	 * return if the parameter respect the rule
	 */
	@Override
	public boolean isValid(IParameter parameter) {
		if (!parameter.isSet()) return true;
		return !(parameter.getParameterValue() == null || parameter.getParameterValue().trim().isEmpty());
	}
	/**
	 * Get the type of the validation
	 */
	@Override
	public JCDKValidation getType() {
		return _type;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RequireValue)) {
			return false;
		}
		return true;
	}

}
