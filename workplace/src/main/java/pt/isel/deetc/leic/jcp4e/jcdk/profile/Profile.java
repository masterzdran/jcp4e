/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 *
 */
package pt.isel.deetc.leic.jcp4e.jcdk.profile;

import java.io.File;
import java.io.FilenameFilter;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKTypes;

/**
 * 
 * Represent an instance of JCDK Profile
 *
 */
@XmlRootElement(name="profile")
public class Profile implements IProfile {

	private String homePath;
	private String binPath;
	private String libPath;
	private JCDKTypes type;
	private String profileLabel;
	private String filename;
	private String version;
	private String id;
	private boolean defaultProfile;
	
	private String simulator;
	private String emulator;
	
	private String _classPathFiles;
	
	public Profile() {
		this(null);
	}
	public Profile(String filename) {
		this.filename=filename;
		defaultProfile=false;
	}
	
	/**
	 * Get the Home Path of JCDK Profile
	 */
	@XmlAttribute
	@Override
	public String getHomePath() {
		return homePath;
	}
	
	/**
	 * Set the Home Path of JCDK Profile
	 */
	@Override
	public void setHomePath(String homePath) {
		this.homePath = homePath;
	}
	/**
	 * Get the Bin Path of JCDK Profile
	 */
	@XmlAttribute
	@Override
	public String getBinPath() {
		return binPath;
	}
	/**
	 * Set the Bin Path of JCDK Profile
	 */
	@Override
	public void setBinPath(String binPath) {
		this.binPath = binPath;
	}
	/**
	 * Get the Lib Path of JCDK Profile
	 */
	@XmlAttribute
	@Override
	public String getLibPath() {
		return libPath;
	}
	/**
	 * Set the Lib Path of JCDK Profile 
	 */
	@Override
	public void setLibPath(String libPath) {
		this.libPath = libPath;
	}
	
	/**
	 * Return the type of JCDK Profile
	 */
	@XmlAttribute
	@Override
	public JCDKTypes getType() {
		return type;
	}
	/**
	 * Set the JCDK type Profile
	 */
	@Override
	public void setType(JCDKTypes type) {
		this.type = type;
	}
	
	/**
	 * Get the profile label. This label is the text that we wish to put in eclipse IDE options 
	 */
	@XmlAttribute
	@Override
	public String getProfileLabel() {
		return profileLabel;
	}
	/**
	 * Set the profile label. This label is the text that we wish to put in eclipse IDE options 
	 */
	@Override
	public void setProfileLabel(String profileLabel) {
		this.profileLabel = profileLabel;
	}
	/**
	 * Get the name of the filename of the xml that has (or will have) the command collection profile
	 */
	@XmlAttribute
	@Override
	public String getFilename() {
		return filename;
	}
	/**
	 * Get the name of the filename of the xml that has (or will have) the command collection profile
	 */

	@Override
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Get the JCDK Profile Version
	 */
	@XmlAttribute
	@Override
	public String getVersion() {
		return version;
	}
	/**
	 * Set the JCDK Profile Version
	 */
	
	@Override
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * Get identification of the profile. This is should be unique
	 */
	@XmlAttribute
	@Override
	public String getId() {
		return id;
	}
	/**
	 * Set identification of the profile. This is should be unique
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * Tells if this profile is the default profile
	 */
	@XmlAttribute
	@Override
	public boolean isDefaultProfile() {
		return defaultProfile;
	}
	/**
	 * Set the profile default if parameter is true.
	 */
	@Override
	public void setDefaultProfile(boolean defaultProfile) {
		this.defaultProfile = defaultProfile;
	}
	/**
	 * Set All libraries as an command line string classpath.
	 */
	public void setClassPathFiles() {
		_classPathFiles = getClassPathFiles(libPath);
	}
	/**
	 * Return an string with an command line classpath
	 * @return
	 */
	public String getClassPathFiles() {
		if ( _classPathFiles == null)
			this.setClassPathFiles();
		return _classPathFiles;
	}

	/**
	 * Get absolute path for the emulator (runtime)
	 * @return
	 */
	@XmlElement
	public String getEmulator() {
		return emulator;
	}
	/**
	 * Get absolute path for the simulator (runtime)
	 * @return
	 */
	@XmlElement
	public String getSimulator() {
		return simulator;
	}
	
	/**
	 * Set absolute path for the emulator (runtime)
	 * @return
	 */
	public void setEmulator(String emulator) {
		this.emulator = emulator;
	}
	/**
	 * Set absolute path for the simulator (runtime)
	 * @return
	 */
	public void setSimulator(String simulator) {
		this.simulator = simulator;
	}
	
	/**
	 * Return all jar libraries from path
	 * @param path
	 * @return
	 */
	private File[] getJars(String path){
		File libHome=new File(path);
		return libHome.listFiles(
				new FilenameFilter() {			
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".jar");
					}
				}
		);		
	}

	/**
	 * Return an string with the classpath
	 * @param path
	 * @return
	 */
	private String getClassPathFiles(String path)
	{
		StringBuilder classpath = new StringBuilder();
		classpath.append("-classpath ");
		for(File f : getJars(path))
		{
			classpath.append(f.getAbsolutePath()+File.pathSeparatorChar);
		}
		return classpath.toString().substring(0,classpath.toString().length()-1);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((binPath == null) ? 0 : binPath.hashCode());
		result = prime * result + (defaultProfile ? 1231 : 1237);
		result = prime * result
				+ ((emulator == null) ? 0 : emulator.hashCode());
		result = prime * result
				+ ((filename == null) ? 0 : filename.hashCode());
		result = prime * result
				+ ((homePath == null) ? 0 : homePath.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libPath == null) ? 0 : libPath.hashCode());
		result = prime * result
				+ ((profileLabel == null) ? 0 : profileLabel.hashCode());
		result = prime * result
				+ ((simulator == null) ? 0 : simulator.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (binPath == null) {
			if (other.binPath != null)
				return false;
		} else if (!binPath.equals(other.binPath))
			return false;
		if (defaultProfile != other.defaultProfile)
			return false;
		if (emulator == null) {
			if (other.emulator != null)
				return false;
		} else if (!emulator.equals(other.emulator))
			return false;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (homePath == null) {
			if (other.homePath != null)
				return false;
		} else if (!homePath.equals(other.homePath))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libPath == null) {
			if (other.libPath != null)
				return false;
		} else if (!libPath.equals(other.libPath))
			return false;
		if (profileLabel == null) {
			if (other.profileLabel != null)
				return false;
		} else if (!profileLabel.equals(other.profileLabel))
			return false;
		if (simulator == null) {
			if (other.simulator != null)
				return false;
		} else if (!simulator.equals(other.simulator))
			return false;
		if (type != other.type)
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	
}
