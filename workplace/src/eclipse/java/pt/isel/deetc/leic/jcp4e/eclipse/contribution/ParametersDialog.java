/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.contribution;

import java.io.File;
import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import pt.isel.deetc.leic.jcp4e.eclipse.preferences.PreferenceAccess;
import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;
import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IParameterRule;

class ParametersDialog extends Dialog{

	private Command command;
	private Parameter[] parameters;
	
	private BooleanFieldEditor[] booleanEditor;
	private StringFieldEditor[] stringEditor;
	private Set<JCDKValidation> validators;
	private FileFieldEditor file;
	int idx=0;
	
	private String commandLabel;
	private File targetFile;
	private FieldEditorPreferencePage page;
	
	public ParametersDialog(Shell parentShell, String label, Command command) {
		super(parentShell);
		this.command = command;
		this.commandLabel = label;
		int size = command.getParameters().size();
		this.booleanEditor = new BooleanFieldEditor[size];
		this.stringEditor = new StringFieldEditor[size];
		this.parameters = new Parameter[size];
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Set parameters for command " + commandLabel + ":");
		newShell.setSize(500, 400);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		//GridLayout layout= new GridLayout(4, false);
		//sComposite.setLayout(layout);
		GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = GridData.FILL;
        gridData.grabExcessVerticalSpace = true; 
        gridData.verticalAlignment = GridData.FILL;
        
        ScrolledComposite sComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
        sComposite.setLayoutData(gridData);
        
        final Composite contents = new Composite(sComposite, SWT.NONE);
        GridLayout layout= new GridLayout();
        layout.numColumns = 4;
        contents.setLayout(layout);
        contents.setLayoutData(new GridData());
        
		page = new FieldEditorPreferencePage(FieldEditorPreferencePage.GRID) {
			@Override
			public void createControl(Composite parentComposite) {
				noDefaultAndApplyButton();
				super.createControl(parentComposite);
			}

			@Override
			protected void createFieldEditors() {
	
				for (Parameter parameter : command.getParameters()) {
					parameters[idx] = parameter;
					Set<IParameterRule> rules = parameter.getParameterRules();
					Set<JCDKValidation> validation = parameter.getValidations();
		
					//create boolean editor and apply true or false 
					createBooleanFieldEditor(parameter.getParameterLabel(), parameter.isSet());
					//create string editor for value
					createStringFieldEditor(parameter.getParameterOption(), parameter.getParameterValue(),
							parameter.getParameterRules());
					idx++;
				}
				
				//create File editor for file to apply command
				//Composite fileContents= new Composite(contents,SWT.BORDER);
				file = new FileFieldEditor("", "File:", contents);
				addField(file);
				
			}

			
			private void createBooleanFieldEditor(String parameterLabel, boolean set) {
				booleanEditor[idx] = new BooleanFieldEditor("", parameterLabel, contents);
				addField(booleanEditor[idx]);
			}

			private void createStringFieldEditor(String parameterOption, String parameterValue, Set<IParameterRule> rules) {
				stringEditor[idx] = new StringFieldEditor("",parameterOption, contents);
				if (parameterValue == null) 
					stringEditor[idx].setStringValue(parameterValue);
				else
					stringEditor[idx].setStringValue("");
				addField(stringEditor[idx]);
				for (IParameterRule rule : rules) {
					if (rule.getType().equals(JCDKValidation.RequireNoValue))
						stringEditor[idx].setEnabled(false, contents);
				}
				}
			};
	
		sComposite.setContent(contents);
		sComposite.setMinSize(480, 380);
		sComposite.setExpandVertical(true);
		sComposite.setExpandHorizontal(true);
		page.createControl(sComposite);
		Control pageControl = page.getControl(); 
        //pageControl.setLayoutData(new GridData(GridData.FILL_BOTH));
        return pageControl;
        }
	
	@Override
	protected void okPressed(){
		idx=0;
		for (BooleanFieldEditor selected : booleanEditor) {
			if (selected.getBooleanValue()){
				parameters[idx].setParameterOption(stringEditor[idx].getStringValue());
			}
			idx++;
		}
		PreferenceAccess.setCommandArguments(file.getStringValue());
		super.okPressed();
	}
}
