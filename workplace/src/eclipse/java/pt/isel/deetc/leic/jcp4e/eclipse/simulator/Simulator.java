/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.simulator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.console.MessageConsoleStream;

import pt.isel.deetc.leic.jcp4e.eclipse.Message;
import pt.isel.deetc.leic.jcp4e.eclipse.consoles.JCP4EConsole;
import pt.isel.deetc.leic.jcp4e.eclipse.logger.Logger;
import pt.isel.deetc.leic.jcp4e.eclipse.preferences.PreferenceAccess;

public class Simulator {

	private static Simulator simulator;
	private Process simProcess;
	private Thread outputDrainer;
	/**
	 * 
	 */
	private Simulator(){
	};
	/**
	 * 
	 * @return
	 */
	public static Simulator getInstance(){
		if (simulator == null)
			simulator = new Simulator();
		return simulator;
	}
	/**
	 * 
	 */
	
	private void simRun(String simPath){
		try {
			//redirect Error Stream to output Stream
			
			simProcess = new ProcessBuilder(simPath.toString()).redirectErrorStream(true).start();
//			//String sim[] = {"cmd.exe", "/C", "dir *"};
//			String sim[] = {"notepad.exe"};
//			
//			simProcess = new ProcessBuilder(sim).redirectErrorStream(true).start();
			
			final InputStream _outputInputStreamReader = simProcess.getInputStream();
			JCP4EConsole console = new JCP4EConsole();
			console.writeConsole(Messages.Simulator_5);
			final MessageConsoleStream consoleStream = console.getConsoleStream(); 
					
			outputDrainer = new Thread(){
			    public void run(){
				    	try{
				            int c;
				            do{
				                c = _outputInputStreamReader.read();
				                if (c >= 0)
				                    consoleStream.write((char)c);
				            }
				            while (c >= 0);
				        }
				        catch (IOException e){
				            e.printStackTrace();
				        }
			    }
			};
			outputDrainer.start();
			simProcess.waitFor();
			
		} catch (IOException  e) {
			Logger.logError(Messages.Simulator_6, e);
			e.printStackTrace();
		}catch (InterruptedException e) {
			Logger.logError(Messages.Simulator_7, e);
			e.printStackTrace();
		}
	}

	
	public void start() {

			//startTest();
			
			final String simPath = PreferenceAccess.getSimulatorPath();
			File file = new File(simPath);
			if (!file.exists()){
				Message.showError(Messages.Simulator_0, Messages.Simulator_1);
				return;
			}				
			
			System.out.println(simPath);
			if (simProcess!=null){
				if(Message.showQuestion(Messages.Simulator_2, Messages.Simulator_3+
								Messages.Simulator_4)){
					stop();
				}else{
					return;
				}
			}
			
			final JCP4EConsole console = new JCP4EConsole();
			
			Job job = new Job("Simulator"){

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					MessageConsoleStream stream= console.getConsoleStream();
					simRun(simPath);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
			
		}
		
	/**
	 * 
	 */
	public void stop(){
		if (simProcess != null){
			simProcess.destroy();
			outputDrainer.interrupt();
			outputDrainer=null;
			simProcess=null;
			writeToConsole(Messages.Simulator_10);
		}else{
			writeToConsole(Messages.Simulator_11);
		}
	}	
	/**
	 * 
	 * @param text
	 */
	private void writeToConsole(String text){
		JCP4EConsole console = new JCP4EConsole();
		console.writeConsole(text);
	}
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings( "unused" ) 
	private MessageConsoleStream getConsoleStream(){
		JCP4EConsole console = new JCP4EConsole();
		return console.getConsoleStream();
	}
	 

}
