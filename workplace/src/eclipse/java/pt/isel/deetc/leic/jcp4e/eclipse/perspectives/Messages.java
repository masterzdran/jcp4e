package pt.isel.deetc.leic.jcp4e.eclipse.perspectives;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.perspectives.messages"; //$NON-NLS-1$
	public static String JCP4EPerspectiveFactory_0;
	public static String JCP4EPerspectiveFactory_1;
	public static String JCP4EPerspectiveFactory_2;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
