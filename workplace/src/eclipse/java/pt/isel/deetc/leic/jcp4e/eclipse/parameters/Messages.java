package pt.isel.deetc.leic.jcp4e.eclipse.parameters;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.parameters.messages"; //$NON-NLS-1$
	public static String ViewListParameters_0;
	public static String ViewListParameters_1;
	public static String ViewListParameters_2;
	public static String ViewListParameters_3;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
