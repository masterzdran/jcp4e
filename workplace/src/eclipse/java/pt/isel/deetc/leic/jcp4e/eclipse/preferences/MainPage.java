/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.preferences;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringButtonFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;
import pt.isel.deetc.leic.jcp4e.eclipse.Message;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileFile;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileUtil;
import pt.isel.deetc.leic.jcp4e.jcdk.JCDKManager;
import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKTypes;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profile;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class MainPage extends PreferencePage implements IWorkbenchPreferencePage {

	public static final String ID = "pt.isel.deetc.leic.jcp4e.eclipse.preferences.MainPage"; //$NON-NLS-1$
	
	private IPreferenceStore store;
	private String oldName;
	private Combo profileComboList;
	private Button addProfile;
	private Button removeProfile;
	
	private int ComboListIndex=0;
	private String[] ComboList;
	
	private StringFieldEditor profileLabel;
	private DirectoryFieldEditor jcdkHomePath;
	private DirectoryFieldEditor jcdkBinPath; 
	private DirectoryFieldEditor jcdkLibPath; 
	private RadioGroupFieldEditor profileType;
	private BooleanFieldEditor profileDefault;
	private FileFieldEditor simulatorFile;
	/*
	 * 
	 */
	public MainPage() {
		super();
		store = Activator.getDefault().getPreferenceStore();
		setPreferenceStore(store);
		setDescription(Messages.MainPage_0);
	}
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		ComboList = ProfileUtil.getProfileNamesList();
		if (ComboList == null){
			return;
		}
		ComboListIndex = store.getInt(PreferenceConstants.JCP4E_PROFILE_SELECTED);
		if (ComboListIndex >= ComboList.length){
			ComboListIndex = ComboList.length;
		}
	}
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
	 */
	protected Control createContents(Composite parent) {
		//create main composite to fill with contents
		Composite top = new Composite(parent, SWT.NONE);
		
		//sets layout for composite in page and inside composite
		top.setLayoutData(new GridData());
		top.setLayout(new GridLayout());
		
		Composite comboGroup = new Composite(top, SWT.NONE);
		comboGroup.setLayoutData(new GridData(SWT.BEGINNING));
		comboGroup.setLayout(new GridLayout());
		
		Label label= new Label(comboGroup, SWT.NONE);
		label.setText(Messages.MainPage_1);
		
		profileComboList = new Combo(comboGroup, SWT.DROP_DOWN | SWT.READ_ONLY | SWT.H_SCROLL);
		if (ComboList != null){
			profileComboList.setItems(ComboList);
			profileComboList.select(ComboListIndex);
		}
		profileComboList.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent event){
				if (profileComboList.getSelectionIndex() == ComboListIndex)
					return;
				comboSelectionChanged();
				profileComboList.pack();
			}
		});
		Composite addRemoveButtons = new Composite(comboGroup,SWT.None);
		
		GridData addRemoveGData = new GridData();
		addRemoveButtons.setLayoutData(addRemoveGData);
		
		GridLayout buttonLayout = new GridLayout();
		buttonLayout.numColumns = 2;
		buttonLayout.marginHeight = 0;
		buttonLayout.marginWidth = 0;
		addRemoveButtons.setLayout(buttonLayout);
		
		addProfile = new Button(addRemoveButtons, SWT.NONE);
		addProfile.setText(Messages.MainPage_2);
		addProfile.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {	
				addProfile();
			}	
		});
		
		removeProfile = new Button(addRemoveButtons, SWT.NONE);
		removeProfile.setText(Messages.MainPage_3);
		removeProfile.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {	
				removeProfile();
			}	
		});
		@SuppressWarnings( "unused" ) 
		Label label1= new Label(addRemoveButtons, SWT.NONE);


		Composite folderGroup = new Composite(top, SWT.NONE);
		folderGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		folderGroup.setLayout(new GridLayout());
		
		Composite labelGroup = new Composite(top, SWT.NONE);
		labelGroup.setLayoutData(new GridData(GridData.BEGINNING));
		labelGroup.setLayout(new GridLayout());
		
		profileLabel = new StringFieldEditor(PreferenceConstants.JCP4E_PROFILE_LABEL, Messages.MainPage_4, 
						StringFieldEditor.UNLIMITED ,StringFieldEditor.VALIDATE_ON_FOCUS_LOST,labelGroup);
		profileLabel.setEmptyStringAllowed(false);
		profileLabel.setPropertyChangeListener(new IPropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				//save value to be processed in performOk.
				oldName = (String) event.getOldValue();
				MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						Messages.MainPage_5, 
						Messages.MainPage_6);
			}
		});
		setFieldEditorsCommonPreferences(profileLabel, store);
		
		Composite pathGroup = new Composite(top, SWT.NONE);
		pathGroup.setLayoutData(new GridData(GridData.BEGINNING));
		pathGroup.setLayout(new GridLayout());
		jcdkHomePath = new DirectoryFieldEditor(PreferenceConstants.JCP4E_JCDK_PATH, Messages.MainPage_7, pathGroup);
		jcdkBinPath = new DirectoryFieldEditor(PreferenceConstants.JCP4E_JCDK_BIN_PATH, Messages.MainPage_8, pathGroup);
		jcdkLibPath = new DirectoryFieldEditor(PreferenceConstants.JCP4E_JCDK_LIB_PATH, Messages.MainPage_9, pathGroup);
		simulatorFile = new FileFieldEditor(PreferenceConstants.JCP4E_PROFILE_SIMULATOR, Messages.MainPage_10, true, StringButtonFieldEditor.VALIDATE_ON_FOCUS_LOST, pathGroup);		
		jcdkHomePath.setEmptyStringAllowed(false);
		jcdkBinPath.setEmptyStringAllowed(false);
		jcdkLibPath.setEmptyStringAllowed(false);
		simulatorFile.setEmptyStringAllowed(false);
		setFieldEditorsCommonPreferences(jcdkHomePath, store);
		setFieldEditorsCommonPreferences(jcdkBinPath, store);
		setFieldEditorsCommonPreferences(jcdkLibPath, store);
		setFieldEditorsCommonPreferences(simulatorFile, store);
		
		@SuppressWarnings( "unused" ) 
		Label label2 = new Label(pathGroup, SWT.NONE);
		
		Composite optionsGroup = new Composite(top, SWT.NONE);
		optionsGroup.setLayoutData(new GridData(GridData.BEGINNING));
		optionsGroup.setLayout(new GridLayout());
		
		String[][] types = getTypes();
		profileType = new RadioGroupFieldEditor(PreferenceConstants.JCP4E_PROFILE_TYPE, Messages.MainPage_11, types.length, 
				types, optionsGroup, true);
		setFieldEditorsCommonPreferences(profileType, store);
		
		profileDefault = new BooleanFieldEditor(PreferenceConstants.JCP4E_PROFILE_DEFAULT, Messages.MainPage_12, optionsGroup);

		setFieldEditorsCommonPreferences(profileDefault, store);
		if (ComboList != null){
			IProfile prof = ProfileUtil.getProfile(ComboList[ComboListIndex]);
			if (prof != null)
				loadValuesFromProfile(prof);	
		}
		loadValues();
		return top;
	}



	@Override
	protected void performDefaults(){
		profileComboList.select(0);
		super.performDefaults();
	}
	
	@Override
	public void performApply(){
		performOk();
	}
	@Override
	public boolean performOk(){
		IProfile profile;
		if (profileComboList.getItemCount()==0){
			//profile file didn't exist
			profile = ProfileUtil.createProfile(profileLabel.getLabelText());
			ProfileFile.getInstance().addProfile(profile);
			profileComboList.add(profileLabel.getStringValue(), 0);
			profileComboList.select(0);
			profileComboList.pack();
		}else if (oldName != null){
				//profile name changed so need to check if its ok
				//if duplicated dont's save and replace old value
				if (ProfileUtil.profileExists(profileLabel.getStringValue())){
					MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						Messages.MainPage_13, Messages.MainPage_14+oldName+Messages.MainPage_15);
					profileLabel.setStringValue(oldName);
				}else{
					//change profile name to new name in combo box
					int idx = profileComboList.getSelectionIndex();
					profileComboList.remove(idx);
					profileComboList.add(profileLabel.getStringValue(), idx);
					profileComboList.select(idx);
				}
			profile = ProfileUtil.getProfile(oldName);
			oldName = null;
			} else {  
				profile = ProfileUtil.getProfile(profileComboList.getText());
		}
		saveValuesToProfile(profile);
		storeValues();
		store.setValue(PreferenceConstants.JCP4E_PROFILE_SELECTED,profileComboList.getSelectionIndex());
		store.setValue(PreferenceConstants.JCP4E_PROFILE_CLASSPATH,((Profile)profile).getClassPathFiles());
		ProfileFile.getInstance().saveProfiles();
		return super.performOk();
	}
	/**
	 * 
	 */
	private void removeProfile() {
		if (!MessageDialog.openConfirm(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
				Messages.MainPage_16, Messages.MainPage_17 +
						Messages.MainPage_18)){
			return;
		};
		JCDKManager manager = ProfileFile.getInstance();
		IProfile profile = ProfileUtil.getProfile(profileComboList.getText());	
		manager.removeProfile(profile);
		if (manager.getAllProfiles().isEmpty()){
			MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					Messages.MainPage_19, Messages.MainPage_20);
			clearAllPreferences();
			return;
		}
		if (profile.isDefaultProfile()){
			//need to change default profile
			manager.getAllProfiles().iterator().next().setDefaultProfile(true);
		}
		profileComboList.remove(ComboListIndex);
		profileComboList.select(0);
		loadValuesFromProfile(ProfileUtil.getProfile(profileComboList.getItem(0)));
		loadValues();
	}
	/**
	 * 
	 */
	private void addProfile() {
		InputDialog dialog = new InputDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
					Messages.MainPage_21, Messages.MainPage_22 , Messages.MainPage_23, new NameValidator());
		if (dialog.open() == org.eclipse.jface.window.Window.CANCEL)
			return;
		
		String newName = dialog.getValue();
		
		ProfileFile.getInstance().addProfile(ProfileUtil.createProfile(newName));
		
		ComboListIndex = profileComboList.getItemCount();
		profileComboList.add(newName,ComboListIndex);
		profileComboList.select(ComboListIndex);
		profileComboList.pack();
		clearAllPreferences();
		store.setValue(PreferenceConstants.JCP4E_PROFILE_LABEL, newName);
		store.setValue(PreferenceConstants.JCP4E_PROFILE_SELECTED, ComboListIndex);
		loadValues();
	}
	/**
	 * 
	 */
	private void comboSelectionChanged() {
		ComboListIndex = profileComboList.getSelectionIndex();
		loadValuesFromProfile(ProfileUtil.getProfile(profileComboList.getItem(ComboListIndex)));
		loadValues();
	}
	/**
	 * 
	 */
	private void clearAllPreferences(){
		store.setValue(PreferenceConstants.JCP4E_PROFILE_LABEL, Messages.MainPage_24);
		store.setValue(PreferenceConstants.JCP4E_JCDK_PATH, Messages.MainPage_25);
		store.setValue(PreferenceConstants.JCP4E_JCDK_LIB_PATH, Messages.MainPage_26);
		store.setValue(PreferenceConstants.JCP4E_JCDK_BIN_PATH, Messages.MainPage_27);
		store.setValue(PreferenceConstants.JCP4E_PROFILE_TYPE, Messages.MainPage_28);
		store.setValue(PreferenceConstants.JCP4E_PROFILE_SELECTED, 0);
		store.setValue(PreferenceConstants.JCP4E_PROFILE_DEFAULT, Messages.MainPage_29);
		store.setValue(PreferenceConstants.JCP4E_PROFILE_SIMULATOR, Messages.MainPage_30);
		store.setValue(PreferenceConstants.JCP4E_PROFILE_CLASSPATH, Messages.MainPage_30);
	}
	/**
	 * 
	 * @param profile
	 */
	private void saveValuesToProfile(IProfile profile){
		profile.setProfileLabel(profileLabel.getStringValue());
		profile.setHomePath(jcdkHomePath.getStringValue());
		profile.setLibPath(jcdkLibPath.getStringValue());
		profile.setBinPath(jcdkBinPath.getStringValue());
		profile.setDefaultProfile(profileDefault.getBooleanValue());
//		if (profile.isDefaultProfile())
//			((Profile)profile).setClassPathFiles();
	}
	/**
	 * 
	 * @param profile
	 */
	private void loadValuesFromProfile(IProfile profile){
		store.setValue(PreferenceConstants.JCP4E_PROFILE_LABEL, profile.getProfileLabel());
		store.setValue(PreferenceConstants.JCP4E_JCDK_PATH, profile.getHomePath());
		store.setValue(PreferenceConstants.JCP4E_JCDK_LIB_PATH, profile.getLibPath());
		store.setValue(PreferenceConstants.JCP4E_JCDK_BIN_PATH, profile.getBinPath());
		store.setValue(PreferenceConstants.JCP4E_PROFILE_DEFAULT, profile.isDefaultProfile());
		store.setValue(PreferenceConstants.JCP4E_PROFILE_CLASSPATH, ((Profile)profile).getClassPathFiles());
	}
	/**
	 * 
	 */
	private void storeValues(){
		profileLabel.store();
		jcdkHomePath.store();
		jcdkBinPath.store(); 
		jcdkLibPath.store(); 
		profileType.store();
		profileDefault.store();
		simulatorFile.store();
	}
	/**
	 * 
	 */
	@SuppressWarnings( "unused" ) 
	private void initializeDefaults(){
		store.setToDefault(PreferenceConstants.JCP4E_PROFILE_LABEL);
		store.setToDefault(PreferenceConstants.JCP4E_JCDK_PATH);
		store.setToDefault(PreferenceConstants.JCP4E_JCDK_BIN_PATH);
		store.setToDefault(PreferenceConstants.JCP4E_JCDK_LIB_PATH);
		store.setToDefault(PreferenceConstants.JCP4E_PROFILE_TYPE);
		store.setToDefault(PreferenceConstants.JCP4E_PROFILE_DEFAULT);
		store.setToDefault(PreferenceConstants.JCP4E_PROFILE_SIMULATOR);
		store.setToDefault(PreferenceConstants.JCP4E_PROFILE_CLASSPATH);
	}
	/*+
	 * 
	 */
	private void loadValues(){
		profileLabel.load();
		jcdkHomePath.load();
		jcdkBinPath.load(); 
		jcdkLibPath.load(); 
		profileType.load();
		profileDefault.load();
		simulatorFile.load();
	}
	/*+
	 * 
	 */
	//transform enum values into a String array necessary for radio selection
	private String[][] getTypes(){
		int enumQty = JCDKTypes.values().length;
		String[][] list = new String[enumQty][enumQty];
		for (int i=0; i<enumQty; ++i){
			list[i][0] = JCDKTypes.values()[i].name();
			list[i][1] = JCDKTypes.values()[i].name();
		}
		return list;
	}
	/**
	 * 
	 * @param editor
	 * @param str
	 */
	//necessary because were using FieldEditors on PreferencePage
	private void setFieldEditorsCommonPreferences(FieldEditor editor, IPreferenceStore str){
		editor.setPage(this);
		editor.setPreferenceStore(str);
		editor.load();
	}

	private class NameValidator implements IInputValidator{

		@Override
		public String isValid(String newText) {
			if (newText.isEmpty()){
				return Messages.MainPage_31;
			}
			if (ProfileUtil.profileExists(newText)){
				return Messages.MainPage_32;
			}
			//Input OK
			return null;
		}
	}

}
