/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.consoles;


import java.io.IOException;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class JCP4EConsole  {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "pt.isel.deetc.leic.jcp4e.eclipse.views.ApduToolView"; //$NON-NLS-1$
	private static final String JCP4EConsole = Messages.JCP4EConsole_1;
	
	private MessageConsole myConsole;
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	private MessageConsole findConsole(String name) {
		IConsoleManager consoleManager =  ConsolePlugin.getDefault().getConsoleManager();
		IConsole[] existing = consoleManager.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		//no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		consoleManager.addConsoles(new IConsole[] { myConsole } );
		consoleManager.showConsoleView(myConsole);
		return myConsole;
	}
	/**
	 * 
	 * @return
	 */
	public MessageConsole getConsole(){
		return myConsole;
	}
	/**
	 * 
	 * @param output
	 * @return
	 */
	public boolean writeConsole(String output){
		final MessageConsoleStream out = myConsole.newMessageStream();
		out.println(output);
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}
	/**
	 * 
	 * @return
	 */
	public MessageConsoleStream getConsoleStream(){
		return myConsole.newMessageStream();
	}

	/**
	 * The constructor.
	 */
	public JCP4EConsole() {
		myConsole = findConsole(JCP4EConsole);
		
	}

	
}