/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;

import pt.isel.deetc.leic.jcp4e.eclipse.logger.Logger;
import pt.isel.deetc.leic.jcp4e.eclipse.perspectives.JCP4EPerspectiveFactory;

public class ChangePerspectiveHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public ChangePerspectiveHandler() {
	}

	/**
	 * 
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		MessageDialog dialog = new MessageDialog(window.getShell(), Messages.ChangePerspectiveHandler_0, 
				null, Messages.ChangePerspectiveHandler_1,	MessageDialog.QUESTION, 
				new String[]{IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL},0);
		
		//answer NO
		if (dialog.open()!=0)
			return null;
		try {
			PlatformUI.getWorkbench().showPerspective(JCP4EPerspectiveFactory.ID,window);
		} catch (WorkbenchException e) {
			//If we receive error log it
			Logger.logError(Messages.ChangePerspectiveHandler_2, e);
		}
		
		return null;
	}

}
