package pt.isel.deetc.leic.jcp4e.eclipse.consoles;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.consoles.messages"; //$NON-NLS-1$
	public static String JCP4EConsole_1;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
