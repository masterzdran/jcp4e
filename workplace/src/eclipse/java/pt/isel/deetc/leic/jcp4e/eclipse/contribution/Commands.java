/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.contribution;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Scrollable;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.dialogs.PreferencesUtil;

import pt.isel.deetc.leic.jcp4e.eclipse.Message;
import pt.isel.deetc.leic.jcp4e.eclipse.consoles.JCP4EConsole;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileUtil;
import pt.isel.deetc.leic.jcp4e.eclipse.logger.Logger;
import pt.isel.deetc.leic.jcp4e.eclipse.preferences.MainPage;
import pt.isel.deetc.leic.jcp4e.eclipse.preferences.PreferenceAccess;
import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;
import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IParameterRule;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profile;

public class Commands extends ContributionItem {
	/**
	 * 
	 */
	public Commands() {
	}
	/**
	 * 
	 * @param id
	 */
	public Commands(String id) {
		super(id);
	}

	@Override
	public boolean isDynamic() {
		return true;
	}

	@Override
	public void fill(Menu menu, int index) {
		String[] clabels = ProfileUtil.getCommandLabels();

 		MenuItem menuItem;

 		if(clabels==null){
			menuItem = new MenuItem(menu, SWT.CHECK, index);
			menuItem.setText(Messages.Commands_4);
			menuItem.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					//what to do when there is no profile selected
					PreferenceDialog pref =	PreferencesUtil.createPreferenceDialogOn(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
							MainPage.ID, null, null);
					if (pref != null)
						pref.open();
				}
			});
		return;
 		}
			//create the menu items
			for (String str : clabels) {
				menuItem = new MenuItem(menu, SWT.CHECK, index);
				menuItem.setText(str);
				menuItem.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						//what to do when menu is subsequently selected.
						executeCommand(((MenuItem)e.getSource()).getText());
					}
				});
			}
	}		
	
	/**
	 * 
	 * @param text
	 */
	private void executeCommand(String text) {
		Command command = (Command)ProfileUtil.getCommandByLabel(text);
		if (command == null){
			Logger.logWarning(Messages.Commands_0+text);
		}
		
		try {
			command.setClassPathFiles(PreferenceAccess.getClasspath());

			JCP4EConsole console = new JCP4EConsole();
			console.writeConsole(Messages.Commands_2+command.getCommandLabel());
			final MessageConsoleStream consoleStream = console.getConsoleStream();
					
//			ParametersWizard wizard = new ParametersWizard();
//			wizard.init(command.getCommandLabel(), command.getParameters());
//		    WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), wizard);
//			dialog.create();
//			dialog.open();
			
			ParametersDialog dialog = new ParametersDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					command.getName(), command);
			dialog.open();
			
			command.execute();
			final BufferedReader _outputInputStreamReader = command.getCommandOutput();
			final BufferedReader _outputErrorStreamReader = command.getCommandErrorOutput();
			
			Thread outputReader = new Thread(){
			    public void run(){
			        while (true){
				    	try{
				            int c;
				            do{
				                c = _outputInputStreamReader.read();
				                if (c >= 0)
				                    consoleStream.write((char)c);
				            }
				            while (c >= 0);
				        }
				        catch (IOException e){
				            e.printStackTrace();
				        }
				    }
			    }
			};
			
			Thread errorReader = new Thread(){
			    public void run(){
			        while (true){
				    	try{
				            int c;
				            do{
				                c = _outputErrorStreamReader.read();
				                if (c >= 0)
				                    consoleStream.write((char)c);
				            }
				            while (c >= 0);
				        }
				        catch (IOException e){
				            e.printStackTrace();
				        }
				    }
			    }
			};
			outputReader.start();
			errorReader.start();
//			Process proc = command.getExecuterProcess();
//			if (proc != null)
//				proc.wait();
			
		} catch (Exception e) {
			Logger.logError(Messages.Commands_3, e);
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	}
	



	