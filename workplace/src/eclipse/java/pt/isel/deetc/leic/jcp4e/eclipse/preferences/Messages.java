package pt.isel.deetc.leic.jcp4e.eclipse.preferences;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.preferences.messages"; //$NON-NLS-1$
	public static String CommandPage_0;
	public static String CommandPage_1;
	public static String CommandPage_10;
	public static String CommandPage_11;
	public static String CommandPage_12;
	public static String CommandPage_13;
	public static String CommandPage_14;
	public static String CommandPage_15;
	public static String CommandPage_16;
	public static String CommandPage_17;
	public static String CommandPage_18;
	public static String CommandPage_19;
	public static String CommandPage_2;
	public static String CommandPage_20;
	public static String CommandPage_21;
	public static String CommandPage_22;
	public static String CommandPage_23;
	public static String CommandPage_24;
	public static String CommandPage_25;
	public static String CommandPage_26;
	public static String CommandPage_27;
	public static String CommandPage_28;
	public static String CommandPage_29;
	public static String CommandPage_3;
	public static String CommandPage_30;
	public static String CommandPage_4;
	public static String CommandPage_5;
	public static String CommandPage_6;
	public static String CommandPage_7;
	public static String CommandPage_8;
	public static String CommandPage_9;
	public static String MainPage_0;
	public static String MainPage_1;
	public static String MainPage_10;
	public static String MainPage_11;
	public static String MainPage_12;
	public static String MainPage_13;
	public static String MainPage_14;
	public static String MainPage_15;
	public static String MainPage_16;
	public static String MainPage_17;
	public static String MainPage_18;
	public static String MainPage_19;
	public static String MainPage_2;
	public static String MainPage_20;
	public static String MainPage_21;
	public static String MainPage_22;
	public static String MainPage_23;
	public static String MainPage_24;
	public static String MainPage_25;
	public static String MainPage_26;
	public static String MainPage_27;
	public static String MainPage_28;
	public static String MainPage_29;
	public static String MainPage_3;
	public static String MainPage_30;
	public static String MainPage_31;
	public static String MainPage_32;
	public static String MainPage_4;
	public static String MainPage_5;
	public static String MainPage_6;
	public static String MainPage_7;
	public static String MainPage_8;
	public static String MainPage_9;
	public static String PreferenceInitializer_0;
	public static String PreferenceInitializer_1;
	public static String PreferenceInitializer_2;
	public static String PreferenceInitializer_3;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}

