/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.handlers;

import java.io.BufferedReader;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.MessageConsoleStream;

import pt.isel.deetc.leic.jcp4e.eclipse.consoles.JCP4EConsole;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileFile;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileUtil;
import pt.isel.deetc.leic.jcp4e.eclipse.logger.Logger;
import pt.isel.deetc.leic.jcp4e.eclipse.preferences.PreferenceAccess;
import pt.isel.deetc.leic.jcp4e.eclipse.simulator.Simulator;
import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;

public class ApduToolHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		InputDialog dialog = new InputDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				Messages.ApduToolHandler_0, Messages.ApduToolHandler_1 , Messages.ApduToolHandler_2, new ApduValidator());
		if (dialog.open() == org.eclipse.jface.window.Window.CANCEL){
			return null;
		}
		
		try{
			JCP4EConsole console = new JCP4EConsole();
			ProfileFile.getInstance().loadCommands(ProfileUtil.getDefaultProfile());
			Command command = (Command)ProfileUtil.getCommandByName(Messages.ApduToolHandler_3);
			if (command == null){
				console.writeConsole(Messages.ApduToolHandler_4);
				return null;
			}
			//Simulator.getInstance().start();
			command.setClassPathFiles(PreferenceAccess.getLibPath());
			command.execute();
			
			final BufferedReader _outputInputStreamReader = command.getCommandOutput();
			final BufferedReader _outputErrorStreamReader = command.getCommandErrorOutput();
			console.writeConsole(Messages.ApduToolHandler_5+command.getCommandLabel());
			final MessageConsoleStream consoleStream = console.getConsoleStream();
			Thread outputDrainer = new Thread(){
			    public void run(){
			        while (true){
				    	try{
				            int c;
				            do{
				                c = _outputInputStreamReader.read();
				                if (c >= 0)
				                    consoleStream.write((char)c);
				            }
				            while (c >= 0);
				            do{
				                c = _outputErrorStreamReader.read();
				                if (c >= 0)
				                    consoleStream.write((char)c);
				            }
				            while (c >= 0);
				        }
				        catch (IOException e){
				            e.printStackTrace();
				        }
				    }
			    }
			};
			outputDrainer.start();
		} catch (Exception e) {
		Logger.logError(Messages.ApduToolHandler_6, e);
		e.printStackTrace();
	}
		return null;
	}
	private class ApduValidator implements IInputValidator{

		@Override
		public String isValid(String newText) {
			if (newText.isEmpty()){
				return Messages.ApduToolHandler_7;
			}
			return null;
		}
	}

}
