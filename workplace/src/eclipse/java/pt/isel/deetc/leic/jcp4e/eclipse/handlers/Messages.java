package pt.isel.deetc.leic.jcp4e.eclipse.handlers;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.handlers.messages"; //$NON-NLS-1$
	public static String ApduToolHandler_0;
	public static String ApduToolHandler_1;
	public static String ApduToolHandler_2;
	public static String ApduToolHandler_3;
	public static String ApduToolHandler_4;
	public static String ApduToolHandler_5;
	public static String ApduToolHandler_6;
	public static String ApduToolHandler_7;
	public static String ChangePerspectiveHandler_0;
	public static String ChangePerspectiveHandler_1;
	public static String ChangePerspectiveHandler_2;
	public static String ShowViewHandler_0;
	public static String ShowViewHandler_1;
	public static String ShowViewHandler_2;
	public static String SimulatorHandler_0;
	public static String SimulatorHandler_1;
	public static String SimulatorHandler_2;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
