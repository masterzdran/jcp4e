/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.preferences;

import java.util.ArrayList;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;
import pt.isel.deetc.leic.jcp4e.eclipse.Message;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileFile;
import pt.isel.deetc.leic.jcp4e.eclipse.controler.ProfileUtil;
import pt.isel.deetc.leic.jcp4e.jcdk.JCDKManager;
import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;
import pt.isel.deetc.leic.jcp4e.jcdk.command.ICommand;
import pt.isel.deetc.leic.jcp4e.jcdk.command.IGuiCommand;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class CommandPage extends PreferencePage implements IWorkbenchPreferencePage {

	public static final String ID = "pt.isel.deetc.leic.jcp4e.eclipse.preferences.CommandPage"; //$NON-NLS-1$
	
	private IPreferenceStore store;
	private TableViewer viewer;
	private Button addCommand;
	private Button removeCommand;
	private Button reloadCommand;
	
	private String[] profileNames;
	private String profileName;
	private Action doubleClickAction;
	private boolean noProfile;
	/**
	 * 
	 */
	public CommandPage() {
		super();
		noDefaultAndApplyButton();
		store = Activator.getDefault().getPreferenceStore();
		setPreferenceStore(store);
		setDescription(Messages.CommandPage_0);
	}
	
	

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		//get profile selected on profile detail page and load commands from file
		profileNames = ProfileUtil.getProfileNamesList();
		if (profileNames == null){
			noProfile = true;
			profileNames = new String[1];
			profileName = profileNames[0] = "no profile available";
			return;
		}
		int idx = store.getInt(PreferenceConstants.JCP4E_PROFILE_SELECTED);
		if (idx >= profileNames.length)
			idx = profileNames.length;
		profileName = ProfileUtil.getProfileNamesList()[idx];
		IProfile profile = ProfileUtil.getProfile(profileName);
		ProfileFile.getInstance().loadCommands(profile);
	}

	@Override
	protected Control createContents(Composite parent) {
		//create main composite to fill with contents
		Composite top = new Composite(parent, SWT.NONE);
		top.setLayoutData(new GridData());
		top.setLayout(new GridLayout(1, false));
		
		Composite title = new Composite(top, SWT.BORDER);
		title.setLayoutData(new GridData());
		title.setLayout(new GridLayout(2, false));
		
		Label label = new Label(title, SWT.NONE);
		label.setText(Messages.CommandPage_1);
		Label label1 = new Label(title, SWT.NONE);
		label1.setText(profileName);
		
		Composite tableComposite = new Composite(top, SWT.BORDER);
		tableComposite.setLayoutData(new GridData());
		tableComposite.setLayout(new GridLayout(2, false));
		
		viewer = new TableViewer(tableComposite, SWT.MULTI | SWT.H_SCROLL| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		createColumns(parent);
		
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		//content provider provided by eclipse
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		//load data to table
		if (!noProfile){
			ArrayList<ICommand>list = new ArrayList<ICommand>(ProfileFile.getInstance().getAllCommands());
			if (list.isEmpty()){
				list.add(new Command(Messages.CommandPage_2, Messages.CommandPage_3, Messages.CommandPage_4));
				list.add(new Command(Messages.CommandPage_5, Messages.CommandPage_6, Messages.CommandPage_7));
				list.add(new Command(Messages.CommandPage_8, Messages.CommandPage_9, Messages.CommandPage_10));
			}
				
			viewer.setInput(list);
		}
		
		//set the table layout
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		viewer.getControl().setLayoutData(gridData);
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				if (selection.getFirstElement() == null  || noProfile){
					removeCommand.setEnabled(false);
				}else{
					removeCommand.setEnabled(true);
				}
			}

			
		});
		
		Composite addRemoveButtons = new Composite(top,SWT.None);
		
		GridData addRemoveGData = new GridData();
		addRemoveButtons.setLayoutData(addRemoveGData);
		
		GridLayout buttonLayout = new GridLayout();
		buttonLayout.numColumns = 2;
		buttonLayout.marginHeight = 0;
		buttonLayout.marginWidth = 0;
		addRemoveButtons.setLayout(buttonLayout);
		
		addCommand = new Button(addRemoveButtons, SWT.NONE);
		addCommand.setText(Messages.CommandPage_11);
		if (noProfile)
			addCommand.setEnabled(false);
		addCommand.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {	
				addCommand();
			}	
		});
		
		removeCommand = new Button(addRemoveButtons, SWT.NONE);
		removeCommand.setText(Messages.CommandPage_12);
		removeCommand.setEnabled(false);
		removeCommand.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {	
				removeCommand();
			}	
		});
		
		reloadCommand = new Button(addRemoveButtons, SWT.NONE);
		reloadCommand.setText(Messages.CommandPage_13);
		reloadCommand.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {	
				reloadCommandList();
			}	
		});
		@SuppressWarnings( "unused" )
		Label label2= new Label(addRemoveButtons, SWT.NONE);
		
		makeActions();
		return top;
	}
	/**
	 * 
	 */
	private void makeActions(){
		doubleClickAction = new Action() {
				public void run() {
					ISelection selection = viewer.getSelection();
					Object obj = ((IStructuredSelection)selection).getFirstElement();
					MessageDialog.openInformation(
							viewer.getControl().getShell(),
							Messages.CommandPage_14,
							Messages.CommandPage_15+obj.toString());
						}
				};
	}
	/**
	 * 
	 */
	private void addCommand(){
		InputDialog dialog = new InputDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				Messages.CommandPage_16, Messages.CommandPage_17 , Messages.CommandPage_18, new NameValidator());
		if (dialog.open() == org.eclipse.jface.window.Window.CANCEL)
			return;
		
		Command newCom = new Command();
		newCom.setName(dialog.getValue());
		
		dialog = new InputDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				Messages.CommandPage_19, Messages.CommandPage_20 , newCom.getName(), null);
		dialog.open();
		newCom.setCommandLabel(dialog.getValue());
		
		dialog = new InputDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				Messages.CommandPage_21, Messages.CommandPage_22 , Messages.CommandPage_23, null);
		dialog.open();
		newCom.setCommandEntryPoint(dialog.getValue());
		
		ProfileFile.getInstance().addCommand(newCom);
		viewer.setInput(null);
		viewer.setInput(new ArrayList<ICommand>(ProfileFile.getInstance().getAllCommands()));
	}
	/**
	 * 
	 */
	private void removeCommand(){
		if (!Message.showQuestion(Messages.CommandPage_24, Messages.CommandPage_25)){
			return;
		}
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		Command c = (Command) selection.getFirstElement();
		ProfileFile.getInstance().removeCommand(ProfileUtil.getCommandByName(c.getName()));
		viewer.setInput(null);
		viewer.setInput(new ArrayList<ICommand>(ProfileFile.getInstance().getAllCommands()));
	}
	/**
	 * 
	 */
	public void reloadCommandList(){
		if (noProfile)
			return;
		profileName = ProfileUtil.getProfileNamesList()[store.getInt(PreferenceConstants.JCP4E_PROFILE_SELECTED)];
		IProfile profile = ProfileUtil.getProfile(profileName);
		ProfileFile.getInstance().loadCommands(profile);
		viewer.setInput(null);
		viewer.setInput(new ArrayList<ICommand>(ProfileFile.getInstance().getAllCommands()));
	}
	/**
	 * 
	 * @param parent
	 */
	private void createColumns(final Composite parent){
		String[] titles = {Messages.CommandPage_26, Messages.CommandPage_27, Messages.CommandPage_28};
		int [] widths = {100, 100, 100};
		
		TableViewerColumn col = createTableViewerColumn(titles[0], widths[0], 0);
		col.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element){
				Command c = (Command) element;
				return c.getCommandLabel();
			}
		});
		
		col = createTableViewerColumn(titles[1], widths[1], 1);
		col.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element){
				Command c = (Command) element;
				return c.getName();
			}
		});
		col = createTableViewerColumn(titles[2], widths[2], 2);
		col.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element){
				Command c = (Command) element;
				return c.getCommandEntryPoint();
			}
		});
			
	}
	/**
	 * 
	 * @param title
	 * @param width
	 * @param colNumber
	 * @return
	 */
	private TableViewerColumn createTableViewerColumn(String title, int width, int colNumber){
		TableViewerColumn vc = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn c = vc.getColumn();
		c.setText(title);
		c.setWidth(width);
		c.setResizable(true);
		c.setMoveable(true);
		return vc;
	}
	
	@Override
	public boolean performOk(){
		ProfileFile.getInstance().saveCommands();
		return true;
	}
	
	
	private class NameValidator implements IInputValidator{

		@Override
		public String isValid(String newText) {
			if (newText.isEmpty()){
				return Messages.CommandPage_29;
			}
			if (ProfileUtil.profileExists(newText)){
				return Messages.CommandPage_30;
			}
			//Input OK
			return null;
		}
	}
	
	
	
}