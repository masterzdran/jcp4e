/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.preferences;

import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;
import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKTypes;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

	
	public PreferenceInitializer() {}
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		store.setDefault(PreferenceConstants.JCP4E_JCDK_PATH, Activator.getConfigDir().addTrailingSeparator().toOSString());
		store.setDefault(PreferenceConstants.JCP4E_JCDK_BIN_PATH, Activator.getConfigDir().addTrailingSeparator().toOSString());
		store.setDefault(PreferenceConstants.JCP4E_JCDK_LIB_PATH, Activator.getConfigDir().addTrailingSeparator().toOSString());
		store.setDefault(PreferenceConstants.JCP4E_PROFILE_LABEL, Messages.PreferenceInitializer_2);
		store.setDefault(PreferenceConstants.JCP4E_PROFILE_TYPE, JCDKTypes.values()[0].name());
		store.setDefault(PreferenceConstants.JCP4E_PROFILE_DEFAULT, true);
		Path p = new Path(store.getDefaultString(PreferenceConstants.JCP4E_JCDK_BIN_PATH));
		p.addTrailingSeparator().append(Messages.PreferenceInitializer_3);
		store.setDefault(PreferenceConstants.JCP4E_PROFILE_SIMULATOR, p.toOSString());
		store.setDefault(PreferenceConstants.JCP4E_PROFILE_CLASSPATH, "");
		store.setDefault(PreferenceConstants.JCP4E_PROFILE_SELECTED, 0);
		store.setDefault(PreferenceConstants.JCP4E_COMMAND_ARGUMENTS, "");
	}
}
	
