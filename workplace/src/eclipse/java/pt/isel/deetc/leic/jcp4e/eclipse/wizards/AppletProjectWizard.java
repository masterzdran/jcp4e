/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.wizards;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;
import pt.isel.deetc.leic.jcp4e.eclipse.Message;

public class AppletProjectWizard extends Wizard implements INewWizard {

	private WizardNewProjectCreationPage page;
	private IConfigurationElement config;
	private IWorkbench workbench;
	private IProject project;
	/**
	 * 
	 */
	public AppletProjectWizard() {
		super();
		//set window title
		setWindowTitle(Messages.AppletProjectWizard_0);
	}
	/**
	 * 
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
	}
	/**
	 * 
	 */
	@Override	
	public void addPages(){
		super.addPages();
		page = new WizardNewProjectCreationPage(Messages.AppletProjectWizard_1);
		page.setTitle(Messages.AppletProjectWizard_2);
		page.setDescription(Messages.AppletProjectWizard_3);
		addPage(page);
		
	}
	/**
	 * 
	 */
	@Override
	public boolean performFinish() {

		if (project != null) {
			return true;
		}

		final IProject projectHandle = page.getProjectHandle();

		URI projectURI = (!page.useDefaults()) ? page.getLocationURI() : null;

		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		final IProjectDescription desc = workspace
				.newProjectDescription(projectHandle.getName());

		desc.setLocationURI(projectURI);

		/*
		 * Just like the NewFileWizard, but this time with an operation object
		 * that modifies workspaces.
		 */
		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
			protected void execute(IProgressMonitor monitor)
					throws CoreException {
				createProject(desc, projectHandle, monitor);
			}
		};

		/*
		 * This isn't as robust as the code in the BasicNewProjectResourceWizard
		 * class. Consider beefing this up to improve error handling.
		 */
		try {
			getContainer().run(true, true, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			Message.showError(Messages.AppletProjectWizard_4, realException.getMessage());
			return false;
		}

		project = projectHandle;

		if (project == null) {
			return false;
		}

		BasicNewProjectResourceWizard.updatePerspective(config);
		BasicNewProjectResourceWizard.selectAndReveal(project, workbench
				.getActiveWorkbenchWindow());

		return true;
	}

	/**
	 * This creates the project in the workspace.
	 * 
	 * @param description
	 * @param projectHandle
	 * @param monitor
	 * @throws CoreException
	 * @throws OperationCanceledException
	 */
	void createProject(IProjectDescription description, IProject proj,
			IProgressMonitor monitor) throws CoreException,
			OperationCanceledException {
		try {

			monitor.beginTask(Messages.AppletProjectWizard_5, 2000);

			proj.create(description, new SubProgressMonitor(monitor, 1000));

			if (monitor.isCanceled()) {
				throw new OperationCanceledException();
			}

			proj.open(IResource.BACKGROUND_REFRESH, new SubProgressMonitor(
					monitor, 1000));

			/*
			 * Okay, now we have the project and we can do more things with it
			 * before updating the perspective.
			 */
			IContainer container = (IContainer) proj;

			/* Add an XHTML file */
			addFileToProject(container, new Path(Messages.AppletProjectWizard_6),
					openContentStream(Messages.AppletProjectWizard_7+ proj.getName()), monitor);

			/* Add the style folder and the site.css file to it */
			final IFolder styleFolder = container.getFolder(new Path(Messages.AppletProjectWizard_8));
			styleFolder.create(true, true, monitor);
			
			InputStream resourceStream = this.getClass().getResourceAsStream(
			Messages.AppletProjectWizard_9);

			addFileToProject(container, new Path(styleFolder.getName()
					+ Path.SEPARATOR + Messages.AppletProjectWizard_10),
					resourceStream, monitor);

			resourceStream.close();
			
			/*
			 * Add the images folder, which is an official Exmample.com standard
			 * for static web projects.
			 */
			IFolder imageFolder = container.getFolder(new Path(Messages.AppletProjectWizard_11));
			imageFolder.create(true, true, monitor);
		} catch (IOException ioe) {
			IStatus status = new Status(IStatus.ERROR, Messages.AppletProjectWizard_12, IStatus.OK,
					ioe.getLocalizedMessage(), null);
			throw new CoreException(status);
		} finally {
			monitor.done();
		}
	}


	/**
	 * Sets the initialization data for the wizard.
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		this.config = config;
	}

	/**
	 * Adds a new file to the project.
	 * 
	 * @param container
	 * @param path
	 * @param contentStream
	 * @param monitor
	 * @throws CoreException
	 */
	private void addFileToProject(IContainer container, Path path,
			InputStream contentStream, IProgressMonitor monitor)
			throws CoreException {
		final IFile file = container.getFile(path);

		if (file.exists()) {
			file.setContents(contentStream, true, true, monitor);
		} else {
			file.create(contentStream, true, monitor);
		}
	}
	public static InputStream openContentStream(String title) throws CoreException {

		/* We want to be truly OS-agnostic */
		final String newline = System.getProperty(Messages.AppletProjectWizard_13);

		String line;
		StringBuffer sb = new StringBuffer();

		try {
			
			IPath path = new Path(Messages.AppletProjectWizard_14);
			URL templateUrl = FileLocator.find(Activator.getDefault().getBundle(),path, Collections.EMPTY_MAP);
			File template = new File(FileLocator.toFileURL(templateUrl).toURI());
	
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(template)));
			try {

				while ((line = reader.readLine()) != null) {
					line = line.replaceAll(Messages.AppletProjectWizard_15, title);
					sb.append(line);
					sb.append(newline);
				}

			} finally {
				reader.close();
			}

		} catch (IOException ioe) {
			IStatus status = new Status(IStatus.ERROR, Messages.AppletProjectWizard_16, IStatus.OK,
					ioe.getLocalizedMessage(), null);
			throw new CoreException(status);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ByteArrayInputStream(sb.toString().getBytes());

	}

}
