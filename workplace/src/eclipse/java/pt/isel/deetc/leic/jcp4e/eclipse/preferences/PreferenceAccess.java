/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.preferences;

import org.eclipse.jface.preference.IPreferenceStore;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;

public class PreferenceAccess {

	private PreferenceAccess(){}
	/**
	 * 
	 * @return
	 */
	public static String getSimulatorPath(){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(PreferenceConstants.JCP4E_PROFILE_SIMULATOR);
	}
	/**
	 * 
	 * @return
	 */
	public static String getHomePath(){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(PreferenceConstants.JCP4E_JCDK_PATH);
	}
	/**
	 * 
	 * @return
	 */
	public static String getLibPath(){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(PreferenceConstants.JCP4E_JCDK_LIB_PATH);
	}
	/**
	 * 
	 * @return
	 */
	public static String getBinPath(){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(PreferenceConstants.JCP4E_JCDK_BIN_PATH);
	}
	/**
	 * 
	 * @return
	 */
	public static String getClasspath(){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(PreferenceConstants.JCP4E_PROFILE_CLASSPATH);
	}
	/**
	 * 
	 * @return
	 */
	public static String getCommandArguments(){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(PreferenceConstants.JCP4E_COMMAND_ARGUMENTS);
	}
	/**
	 */
	public static void setCommandArguments(String str){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setValue(PreferenceConstants.JCP4E_COMMAND_ARGUMENTS,str);
	}
	
}
