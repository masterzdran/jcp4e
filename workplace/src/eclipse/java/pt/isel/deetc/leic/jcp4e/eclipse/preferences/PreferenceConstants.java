/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.preferences;


/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String JCP4E_CHANGE_PERSPECTIVE_ALWAYS = "jcp4e.preferences.changePerspectivePersistent";
	public static final String JCP4E_PROFILE_LABEL = "jcp4e.preferences.jcdk.profile.label";
	public static final String JCP4E_JCDK_PATH = "jcp4e.preferences.jcdk.path";
	public static final String JCP4E_JCDK_BIN_PATH = "jcp4e.preferences.jcdk.bin.path";
	public static final String JCP4E_JCDK_LIB_PATH = "jcp4e.preferences.jcdk.lib.path";
	public static final String JCP4E_PROFILE_TYPE = "jcp4e.preferences.jcdk.profile.type";
	public static final String JCP4E_PROFILE_DEFAULT = "jcp4e.preferences.jcdk.profile.default";
	public static final String JCP4E_PROFILE_SELECTED = "jcp4e.preferences.jcdk.profile.selected";
	public static final String JCP4E_PROFILE_SIMULATOR = "jcp4e.preferences.jcdk.profile.simulator";
	public static final String JCP4E_PROFILE_CLASSPATH = "jcp4e.preferences.jcdk.profile.type";
	public static final String JCP4E_COMMAND_ARGUMENTS = "jcp4e.preferences.jcdk.command.arguments";

}
