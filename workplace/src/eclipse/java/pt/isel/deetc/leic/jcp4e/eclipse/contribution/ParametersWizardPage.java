/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.contribution;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import pt.isel.deetc.leic.jcp4e.jcdk.enums.JCDKValidation;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;
import pt.isel.deetc.leic.jcp4e.jcdk.parameterrules.IParameterRule;

public class ParametersWizardPage extends WizardPage{

	private Set<Parameter> parameters;
	
	private Button[] booleanButton;
	private Label[] nameLabels;
	private Label[] optionLabels;
	private Text[] text;
	
	private ArrayList<Set<JCDKValidation>> validators;
	int idx=0;

	
	private FieldEditorPreferencePage page;
	private String commandLabel;
	private File targetFile;
	
	
	
	public ParametersWizardPage(String commandLabel, Set<Parameter> parameters) {
		super("commandParametersWizzard","Command parameters for "+commandLabel,null);
		this.parameters = parameters;
		this.commandLabel = commandLabel;
		int size = parameters.size();
		this.booleanButton = new Button[size];
		this.nameLabels = new Label[size];
		this.optionLabels = new Label[size];
		this.text = new Text[size];
		this.validators = new ArrayList<Set<JCDKValidation>>();
	}
	
//	@Override
//	protected void configureShell(Shell newShell) {
//		super.configureShell(newShell);
//		newShell.setText("Set parameters for command: "+ commandLabel);
//		//newShell.setSize(500, 55*parameters.size());
//		newShell.setSize(500, 300);
//	}
	
	@Override
	public void createControl(Composite parent) {
		parent.setLayout(new FillLayout());
		
		ScrolledComposite sComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.V_SCROLL);
		
		Composite childComposite = new Composite(sComposite,SWT.NONE);
		GridLayout layout = new GridLayout(4, false);
		childComposite.setLayout(layout);
	
		for (Parameter parameter : parameters) {
			Set<IParameterRule> rules = parameter.getParameterRules();
			//create labels for command names
			nameLabels[idx] = new Label(childComposite, SWT.NONE);
			nameLabels[idx].setText(parameter.getParameterLabel());
			nameLabels[idx].setLayoutData(new GridData());
			//create boolean button and apply true or false 
			booleanButton[idx] = new Button(childComposite, SWT.CHECK);
			booleanButton[idx].setSelection(parameter.isSet());
			booleanButton[idx].setLayoutData(new GridData());
			//create string editor for value
			optionLabels[idx] = new Label(childComposite, SWT.NONE);
			optionLabels[idx].setText(parameter.getParameterOption());
			optionLabels[idx].setLayoutData(new GridData());
			//create 
			text[idx] = new Text(childComposite, SWT.SINGLE);
			if (parameter.getParameterValue() == null)
				text[idx].setText("");
			else
				text[idx].setText(parameter.getParameterValue());
			text[idx].setEnabled(true);
			for (IParameterRule rule : rules) {
				if (rule.getType().equals(JCDKValidation.RequireNoValue))
					text[idx].setEditable(false);
				else
					text[idx].setEditable(true);
			}
			text[idx].setLayoutData(new GridData());
			validators.add(parameter.getValidations());
			
			idx++;
		}

		Label file = new Label(childComposite, SWT.NONE);
		file.setText("File:");
		Text text = new Text(childComposite, SWT.BORDER);
		
		sComposite.setContent(childComposite);
		sComposite.setMinSize(100, 50);
		sComposite.setExpandVertical(true);
		sComposite.setExpandHorizontal(true);
		//sComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		setControl(sComposite);
		setPageComplete(true);

	}

	
}
	