/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.controler;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;
import pt.isel.deetc.leic.jcp4e.jcdk.JCDKManager;

public class ProfileFile {

	private static JCDKManager instance;
	/**
	 * 
	 */
	protected ProfileFile() {
	}
	/**
	 * 
	 * @return
	 */
	public static JCDKManager getInstance(){
		if (instance == null){
			instance = new JCDKManager(Activator.getConfigDir().addTrailingSeparator().toOSString());
		}
		return instance;
	}

}
