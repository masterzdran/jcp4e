package pt.isel.deetc.leic.jcp4e.eclipse.logger;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.logger.messages"; //$NON-NLS-1$
	public static String Logger_0;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
