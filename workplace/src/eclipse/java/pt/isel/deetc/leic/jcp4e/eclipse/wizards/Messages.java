package pt.isel.deetc.leic.jcp4e.eclipse.wizards;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.wizards.messages"; //$NON-NLS-1$
	public static String AppletProjectWizard_0;
	public static String AppletProjectWizard_1;
	public static String AppletProjectWizard_10;
	public static String AppletProjectWizard_11;
	public static String AppletProjectWizard_12;
	public static String AppletProjectWizard_13;
	public static String AppletProjectWizard_14;
	public static String AppletProjectWizard_15;
	public static String AppletProjectWizard_16;
	public static String AppletProjectWizard_2;
	public static String AppletProjectWizard_3;
	public static String AppletProjectWizard_4;
	public static String AppletProjectWizard_5;
	public static String AppletProjectWizard_6;
	public static String AppletProjectWizard_7;
	public static String AppletProjectWizard_8;
	public static String AppletProjectWizard_9;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
