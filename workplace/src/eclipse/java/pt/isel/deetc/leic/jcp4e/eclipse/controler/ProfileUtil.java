/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.controler;

import java.util.Date;
import java.util.Set;

import pt.isel.deetc.leic.jcp4e.jcdk.JCDKManager;
import pt.isel.deetc.leic.jcp4e.jcdk.command.Command;
import pt.isel.deetc.leic.jcp4e.jcdk.command.IGuiCommand;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.IGuiParameter;
import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.IProfile;
import pt.isel.deetc.leic.jcp4e.jcdk.profile.Profile;

public class ProfileUtil {

	private ProfileUtil(){}

	/**
	 * 
	 * @return
	 */
	public static String[] getProfileNamesList(){
		JCDKManager manager = ProfileFile.getInstance();
		
		Set<IProfile> set = manager.getAllProfiles();
		if (set.size() == 0)
			return null;
		String[] plist = new String[set.size()];
		int idx = 0;
		for (IProfile p : set) {
			plist[idx++] = p.getProfileLabel();
		}
		return plist;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean profileExists(String name){
		if (name==null) 
			return false;
		for (String str : getProfileNamesList()) {
			if (str.equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}
	/**
	 * 
	 * @return
	 */
	public static IProfile getDefaultProfile(){
		JCDKManager manager = ProfileFile.getInstance();
		for (IProfile profile : manager.getAllProfiles()) {
			if (profile.isDefaultProfile()){
				return profile;
			}
		}
		return null;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static IProfile getProfile(String name){
		if (name == null)
			return null;
		JCDKManager manager = ProfileFile.getInstance();
		for (IProfile profile : manager.getAllProfiles()) {
			if (name.equalsIgnoreCase(profile.getProfileLabel())){
				return profile;
			}
		}
		return null;
	}
	/**
	 * 
	 * @param profile
	 * @param name
	 * @return
	 */
	public static IProfile changeProfileName(IProfile profile, String name){
		if (name == null)
			return null;
		profile.setProfileLabel(name);
		profile.setFilename(name.replace(" ", ".").toLowerCase()+"-commands.xml");
		return profile;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static IProfile createProfile(String name){
		IProfile profile = new Profile();
		profile.setId(new Date().toString());
		return changeProfileName(profile, name);
	}
	/**
	 * 
	 * @return
	 */
	public static String[] getCommandLabels(){
		ProfileFile.getInstance().loadCommands(ProfileUtil.getDefaultProfile());
		Set<IGuiCommand> commands = ProfileFile.getInstance().getAllCommands();
		if (commands == null){
			return null;
		}
		String[] result = new String[commands.size()];
		int i=0;
		for (IGuiCommand com : commands) {
			result[i++] = com.getCommandLabel(); 
		}
		return result; 
	} 
	/*+
	 * 
	 */
	public static IGuiCommand getCommandByLabel(String label){
		Set<IGuiCommand> commands = ProfileFile.getInstance().getAllCommands();
		for (IGuiCommand com : commands) {
			if (com.getCommandLabel().equalsIgnoreCase(label)){
				return com;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static IGuiCommand getCommandByName(String name){
		Set<IGuiCommand> commands = ProfileFile.getInstance().getAllCommands();
		for (IGuiCommand com : commands) {
			if (com.getName().equalsIgnoreCase(name)){
				return com;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean commandExists(String name){
		Set<IGuiCommand> commands = ProfileFile.getInstance().getAllCommands();
		for (IGuiCommand com : commands) {
			if (com.getName().equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}
	
	public Set<Parameter> getCommandParameters(String label){
		Command command = (Command) getCommandByLabel(label);
		return command.getParameters();
		
		
	}
}


