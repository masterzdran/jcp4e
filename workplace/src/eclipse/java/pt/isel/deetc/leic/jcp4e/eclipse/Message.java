/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse;

import org.eclipse.jface.dialogs.MessageDialog;

import org.eclipse.ui.PlatformUI;

public class Message {
	/**
	 * 
	 * @param title
	 * @param message
	 */
	public static void showInfo(String title, String message){
		MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message);
	}
	/**
	 * 
	 * @param title
	 * @param message
	 */
	public static void showError(String title, String message){
		MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message);
	}
	/**
	 * 
	 * @param title
	 * @param message
	 * @return
	 */
	public static boolean showQuestion(String title, String message){
		return MessageDialog.openQuestion(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message);
	}
	/**
	 * 
	 * @param title
	 * @param message
	 * @return
	 */
	public static boolean showConfirm(String title, String message){
		return MessageDialog.openConfirm(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message);
	}
	/**
	 * 
	 * @param title
	 * @param message
	 */
	public static void showWarning(String title, String message){
		MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message);
	}


}
