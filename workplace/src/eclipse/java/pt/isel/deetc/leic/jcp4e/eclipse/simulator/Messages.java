package pt.isel.deetc.leic.jcp4e.eclipse.simulator;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.simulator.messages"; //$NON-NLS-1$
	public static String Simulator_0;
	public static String Simulator_1;
	public static String Simulator_10;
	public static String Simulator_11;
	public static String Simulator_2;
	public static String Simulator_3;
	public static String Simulator_4;
	public static String Simulator_5;
	public static String Simulator_6;
	public static String Simulator_7;
	public static String Simulator_8;
	public static String Simulator_9;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
