package pt.isel.deetc.leic.jcp4e.eclipse;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "pt.isel.deetc.leic.jcp4e.eclipse.messages"; //$NON-NLS-1$
	public static String Activator_0;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
