/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import pt.isel.deetc.leic.jcp4e.eclipse.logger.Logger;
import pt.isel.deetc.leic.jcp4e.eclipse.views.SimulatorView;

public class ShowViewHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String parameter = event.getParameter(Messages.ShowViewHandler_0);
		
		if (parameter.equalsIgnoreCase(Messages.ShowViewHandler_1)){
			try {
				HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(SimulatorView.ID);
			} catch (PartInitException e) {
				Logger.logError(Messages.ShowViewHandler_2, e);
			}
		}		
		return null;
	}

}
