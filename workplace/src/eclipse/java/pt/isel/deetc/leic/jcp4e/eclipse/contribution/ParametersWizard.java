/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.contribution;

import java.util.Set;

import org.eclipse.core.resources.IFolder;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWizard;

import pt.isel.deetc.leic.jcp4e.jcdk.parameter.Parameter;

public class ParametersWizard extends Wizard implements INewWizard{

	
	public static final String copyright = "(c) Copyright IBM Corporation 2002.";	
	
		ParametersWizardPage page;
		String commandLabel;
		Set<Parameter> parameters;
		


	public ParametersWizard(){
		super();
	}
	
	@Override
	public void addPages(){
		page = new ParametersWizardPage(commandLabel, parameters);
		addPage(page);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub
		
	}
	
	public void init(String commandLabel, Set<Parameter> parameters){
		this.commandLabel = commandLabel;
		this.parameters = parameters;
	}

	@Override
	public boolean canFinish(){
		
		//field validation
		return true;
	}
	
	@Override
	public boolean performFinish() 
	{

		return true;
	}


}


