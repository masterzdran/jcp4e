/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IViewLayout;
import org.eclipse.ui.console.IConsoleConstants;

import pt.isel.deetc.leic.jcp4e.eclipse.consoles.JCP4EConsole;
import pt.isel.deetc.leic.jcp4e.eclipse.views.SimulatorView;

public class JCP4EPerspectiveFactory implements IPerspectiveFactory {

	
	public static String ID = Messages.JCP4EPerspectiveFactory_0;
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	@Override
	public void createInitialLayout(IPageLayout layout) {
		
		//get editor area
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		
		//LEFT: project manager view
		layout.addView(IPageLayout.ID_PROJECT_EXPLORER,IPageLayout.LEFT,0.25f,editorArea);
		
		IFolderLayout bottom = layout.createFolder(Messages.JCP4EPerspectiveFactory_1, 
								IPageLayout.BOTTOM, 
								0.66f, 
								editorArea);
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView(SimulatorView.ID);
//		IViewLayout vl = layout.getViewLayout(SimulatorView.ID);
//		vl.setCloseable(true);
//		vl.setMoveable(true);
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);

		JCP4EConsole JCP4EConsole = new JCP4EConsole();
		JCP4EConsole.writeConsole(Messages.JCP4EPerspectiveFactory_2);

		
	}

}
