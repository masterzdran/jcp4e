/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse;



import java.io.File;
import java.net.URL;

import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "pt.isel.deetc.leic.jcp4e"; //$NON-NLS-1$
	
	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		

		
		
		
		
		

		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	/**
	 * Returns a string with a path to the Config Folder of eclipse.
	 * If folder is read-only returns workspace folder 
	 *
	 * @return String with configuration folder path of the plugin and if read-only the workspace folder
	 */
	public static Path getConfigDir(){
		File configDir;
		Location location = Platform.getConfigurationLocation();
		if (location != null){
			URL configURL = location.getURL();
			if(configURL != null && configURL.getProtocol().startsWith(Messages.Activator_0)){
				configDir = new File(configURL.getFile(), PLUGIN_ID);
				if (configDir != null && !configDir.exists()){
					configDir.mkdirs();
				}
				return new Path(configDir.getAbsolutePath());
			}
		}
		//if location==null then config folder is read-only
		//return alternate location (workspace)
		Bundle bundle = Platform.getBundle(PLUGIN_ID);
		return new Path(Platform.getStateLocation(bundle).toFile().getAbsolutePath());
	}
}
