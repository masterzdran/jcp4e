/**	
* This file is part of JCP4E - Java Card Plugin for Eclipse.
* 
* JCP4E is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* JCP4E is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with JCP4E.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @author Nuno Cancelo (nuno.cancelo@gmail.com)
 * @author Nuno Sousa	(ng.sousa@gmail.com)
 */

package pt.isel.deetc.leic.jcp4e.eclipse.logger;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import pt.isel.deetc.leic.jcp4e.eclipse.Activator;

public class Logger {
	/**
	 * 
	 * @param msg
	 */
	public static void logInfo(String msg){
		log(IStatus.INFO, IStatus.INFO, msg, null);
	}
	/**
	 * 
	 * @param msg
	 */
	public static void logWarning(String msg){
		log(IStatus.WARNING, IStatus.WARNING, msg, null);
	}
/**
 * 
 * @param exception
 */
	public static void logError(Throwable exception){
		logError(Messages.Logger_0, exception);
	}
	/**
	 * 
	 * @param msg
	 * @param exception
	 */
	public static void logError(String msg, Throwable exception){
		log(IStatus.ERROR, IStatus.ERROR,msg, exception);
	}
	/**
	 * 
	 * @param severity
	 * @param code
	 * @param msg
	 * @param exception
	 */
	public static void log(int severity, int code, String msg, Throwable exception){
		log(createStatus(severity, code, msg, exception));
	}
	/**
	 * 
	 * @param severity
	 * @param code
	 * @param msg
	 * @param exception
	 * @return
	 */
	private static IStatus createStatus(int severity, int code, String msg, Throwable exception){
		return new Status(severity, Activator.PLUGIN_ID, code, msg, exception);
	}
	/**
	 * 
	 * @param status
	 */
	private static void log(IStatus status){
		Activator.getDefault().getLog().log(status);
	}
	
}
