\select@language {portuguese}
\contentsline {chapter}{\numberline {I}Resumo}{i}
\contentsline {chapter}{\numberline {II}Agradecimentos}{ii}
\contentsline {chapter}{\IeC {\'I}ndice}{iii}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}
\contentsline {chapter}{\numberline {2}Objetivos}{4}
\contentsline {section}{\numberline {2.1}Funcionalidades Obrigat\IeC {\'o}rias}{4}
\contentsline {section}{\numberline {2.2}Funcionalidades Opcionais}{5}
\contentsline {section}{\numberline {2.3}Licen\IeC {\c c}a}{6}
\contentsline {chapter}{\numberline {3}Arquitetura}{7}
\contentsline {section}{\numberline {3.1}Camada de Liga\IeC {\c c}\IeC {\~a}o JCSDK}{10}
\contentsline {subsection}{\numberline {3.1.1}Descri\IeC {\c c}\IeC {\~a}o das Entidades}{11}
\contentsline {subsection}{\numberline {3.1.2}Persist\IeC {\^e}ncia dos Dados}{13}
\contentsline {section}{\numberline {3.2}Camada de Liga\IeC {\c c}\IeC {\~a}o Eclipse}{13}
\contentsline {section}{\numberline {3.3}Camada de Interliga\IeC {\c c}\IeC {\~a}o}{13}
\contentsline {chapter}{Refer\IeC {\^e}ncias}{14}
\contentsline {chapter}{Lista de Figuras}{15}
