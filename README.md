![The GNU General Public License v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)
This software is licensed under The GNU General Public License v3.0.  




# JCP4E - JavaCard Plugin for Eclipse #
Plugin to help developing application using JavaCard technology in Eclipse IDE. 

Provide support: 

* Multiple JCDKs (JavaCard Development Kits) standard specifications 
* JCDK's simulators integration 
* JCDK's Java debugger integration 
* APDU's send test tool 
* Multiple Applet Template: 
  * Classic ( APDU communication) 
  * RMI 
  * Servlets 
* Client Applications Templates 

## Project Members ##

* Nuno Cancelo (nuno.cancelo@gmail.com / 31401@alunos.isel.pt)
* Nuno Sousa   (ng.sousa@gmail.com / 33595@alunos.isel.pt)

## Project Advisor ##

* Eng. Pedro Sampaio (ISEL - Instituto Superior de Engenharia de Lisboa - Portugal)
* Eng. Rui Joaquim (ISEL - Instituto Superior de Engenharia de Lisboa - Portugal)
